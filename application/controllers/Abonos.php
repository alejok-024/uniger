<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Abonos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Validación de sesión
		if(!$this->session->userdata('logged_in'))
		{
			redirect('/');//Si no hay variable de sesión activa
		}
		else
		{
			$this->load->model('Recibo_Model');//Se instancia el modelo para muebles
			$this->load->model('Abonos_Model');//Se instancia el modelo para muebles
		}
	}

	/**
     *Valida que un campo fecha no se haya ingresado una fecha mayor a la del dia de hoy
     *
     * @param       date  $date
     * @return      true/false
    */
	public function index()
	{
		$this->load->view('pages-abonos-inicio');
	}
	
	/**
     *Valida que un campo fecha no se haya ingresado una fecha mayor a la del dia de hoy
     *
     * @param       date  $date
     * @return      true/false
    */
	public function crear()
	{	
		$datos['recibos'] = $this->Recibo_Model->traer_datos_pdf();

		$this->load->view('pages-abonos-crear', $datos);
	}
	
	/**
     *Valida que un campo fecha no se haya ingresado una fecha mayor a la del dia de hoy
     *
     * @param       date  $date
     * @return      true/false
    */
	public function consultar()
	{	
		$datos['recibos'] = $this->Recibo_Model->traer_datos_pdf();

		$this->load->view('pages-abonos-consultar', $datos);
	}
	
	/**
     *Valida que un campo fecha no se haya ingresado una fecha mayor a la del dia de hoy
     *
     * @param       date  $date
     * @return      true/false
    */
	public function traer_detalle_abono_por_recibo($numero_recibo)
	{	
		$datos['abonos'] = $this->Abonos_Model->traer_abonos_por_recibo($numero_recibo);

		$this->load->view('ajax-abonos-consultar', $datos);
	}
	
	/**
     *Valida que un campo fecha no se haya ingresado una fecha mayor a la del dia de hoy
     *
     * @param       date  $date
     * @return      true/false
    */
	public function eliminar()
	{	
		$abo_id = $this->input->post('abo_id');
		$this->Abonos_Model->eliminar_abono($abo_id);
		echo "borrado";
	}

	/**
     *Valida que un campo fecha no se haya ingresado una fecha mayor a la del dia de hoy
     *
     * @param       date  $date
     * @return      true/false
    */
	public function traer_items_factura($numero_recibo)
	{	
		$this->load->model('Multas_Model');
		$datos['multas'] = $this->Multas_Model->traer_multas_por_recibo($numero_recibo);
		$this->load->model('Cuotas_Extraordinarias_Model');
		$datos['cuotas_extraordinarias'] = $this->Cuotas_Extraordinarias_Model->traer_cuotas_extraordinarias_por_recibo($numero_recibo);
		$this->load->model('Presupuestos_Model');
        $datos['presupuestos'] = $this->Presupuestos_Model->traer_presupuestos_por_recibo($numero_recibo);
        $datos['id_recibo_pago'] = $this->Presupuestos_Model->traer_id_recibo_pago($numero_recibo);
		$datos['dias_vencido'] = $datos['presupuestos'][0]->Rec_Pag_Dias_Vencidos;
		$datos['Abonos'] = $this->Abonos_Model->traer_abonos_por_recibo($numero_recibo);
		
		$this->load->view('ajax-abonos-datos', $datos);

	}

	/**
     *Valida que un campo fecha no se haya ingresado una fecha mayor a la del dia de hoy
     *
     * @param       date  $date
     * @return      true/false
    */
	public function guardar()
	{	
		$this->form_validation->set_error_delimiters('<p class="mt-3 text-danger">', '</p>');
		$this->form_validation->set_rules('input-total-pagar', 'total a pagar', 'trim|required|greater_than[1]|max_length[8]');
        $this->form_validation->set_rules('input-abono', 'valor a abonar', 'trim|required|min_length[1]|max_length[12]|callback_valida_numero|callback_validar_minima_cuantia');
        
        if ($this->form_validation->run() == FALSE) {

			$this->crear();

        } else {

			$valor = $this->remover_formato_numero($this->input->post('input-abono'));
			$rec_pag_id = $this->input->post('input-id-recibo-pago');
			
			$this->Abonos_Model->guardar_abono($valor, $rec_pag_id);
			
			redirect('abonos/consultar/'.$rec_pag_id);
        }

	}

	public function validar_minima_cuantia($valor_abono)
	{
		$valor_total = $this->input->post('input-total-pagar');
		if($valor_abono > $valor_total)
		{
			$this->form_validation->set_message('validar_minima_cuantia', 'El valor a abonar no puede ser mayor al valor que se debe');
			return false;
		}
		else
		{
			return true;
		}
	}

    /**
     *Valida si un campo tiene el formato de la fecha correctamente
     *
     * @param       date  $date
     * @return      true/false
    */
    function remover_formato_numero($text){
        $text = str_replace(".", "", $text);
        return $text;
    }
	
	/**
     *Valida si un campo contiene numeros
     *
     * @param       date  $date
     * @return      true/false
    */
    function valida_numero($text){
        $text = str_replace(".", "", $text);
        if (is_numeric(intval($text))) {
            return true;
        }else
        {
            $this->form_validation->set_message('valida_numero', 'Debe ingresar un valor numérico');
            return false;
        }
    }
}
