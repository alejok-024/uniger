<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensajes extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			//Validación de sesión
			if(!$this->session->userdata('logged_in'))
			{
				redirect('/');//Si no hay variable de sesión activa
			}
			else
			{
				$this->load->model('Mensajes_Model');//Se instancia el modelo para muebles
			}
	}
	public function index()
	{
		$this->load->view('pages-inicio-comunicaciones');
    }
    public function listar_Recibidos()
	{
		$datos['Mensajes']=$this->Mensajes_Model->Listar_mensajes_recibidos();
		$this->load->view('pages-mensajes', $datos);
    }
    public function Redactar_mensaje(){
        $this->load->view('pag-mensajes-redactar');
	}
	public function Mensajes_Enviados(){
		$this->load->view('pages-mensajes-enviados');

	}

}