<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			//Validación de sesión
			if(!$this->session->userdata('logged_in'))
			{
				redirect('/');//Si no hay variable de sesión activa
			}
			else
			{
				$this->load->model('Proveedores_Model');//Se instancia el modelo
			}
	}
	public function index()
	{
		$this->load->view('pages-proveedor');
	}
	public function listar()
	{
		$datos['proveedor']=$this->Proveedores_Model->listar_proveedor();
		$this->load->view('pages-consultar-proveedor', $datos);
	}
	public function crear()
	{
		$datos['proveedores'] = $this->Proveedores_Model->listar_proveedor();
		$this->load->view('pages-registrar-proveedor', $datos);
	}
	public function crear_proveedor()
	{
		//setear validaciones
		$this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
		$this->form_validation->set_rules('Pro_Tipo', 'Tipo de Proveedor', 'required');
		$this->form_validation->set_rules('Pro_Nombre', 'Nombre del Proveedor', 'required');
		$this->form_validation->set_rules('Pro_Telefono', 'Periodo del Proveedor', 'required|numeric');
		$this->form_validation->set_rules('Pro_Direccion', 'Direccion del Proveedor', 'required');
		$this->form_validation->set_rules('Pro_Correo', 'E-mail del Proveedor', 'required|valid_emails');
		$this->form_validation->set_rules('Pro_Contacto', 'Contacto del Proveedor', 'required');
		$this->form_validation->set_rules('Pro_Descripcion', 'Descripción', 'required');

		//hacer validacion
		if ($this->form_validation->run() == FALSE)
		{
				$this->load->view('pages-registrar-proveedor');
		}

		else 
		{
			//Seteo las variables
			$datosProveedor = array
			(
				'Pro_Tipo' => $this->input->post('Pro_Tipo') ,
				'Pro_Nombre' => $this->input->post('Pro_Nombre'),
				'Pro_Telefono' => $this->input->post('Pro_Telefono'),
				'Pro_Direccion' => $this->input->post('Pro_Direccion'),
				'Pro_Correo' => $this->input->post('Pro_Correo'),
				'Pro_Contacto' => $this->input->post('Pro_Contacto'),
				'Pro_Descripcion' => $this->input->post('Pro_Descripcion')
            );

            $result = $this->Proveedores_Model->crear_proveedor($datosProveedor);

            if ($result):
                 redirect('/Proveedores/listar/success');
            endif;
        }
    }

	public function borrar_proveedor(){
		$Token = $this->input->post('Res_Token');
		$result = $this->Proveedores_Model->borrar_proveedor($Token);

		if ($result):
			echo $result;
		endif;
        
	}
	public function editar_proveedor($id){
		$datos['informacion'] = $this->Proveedores_Model->listar_proveedorxId($id);
		$datos['proveedor'] = $this->Proveedores_Model->listar_proveedor();
		$datos['id']= $id;

		if($datos['informacion']){
			$this->load->view('pages-actualizar-proveedor', $datos); //crear la vista
		}else{
			echo "No se encontraron resultados";
		}
	}

	public function actualizar_proveedor(){

		$proveedor_id = $this->input->post('proveedor_id');

		$datosProveedor = array
		(
            'Pro_Tipo' => $this->input->post('Pro_Tipo') ,
            'Pro_Nombre' => $this->input->post('Pro_Nombre'),
            'Pro_Telefono' => $this->input->post('Pro_Telefono'),
            'Pro_Direccion' => $this->input->post('Pro_Direccion'),
			'Pro_Correo' => $this->input->post('Pro_Correo'),
			'Pro_Contacto' => $this->input->post('Pro_Contacto'),
			'Pro_Descripcion' => $this->input->post('Pro_Descripcion')
		);

		$this->Proveedores_Model->actualizar_proveedor($datosProveedor, $proveedor_id);

		redirect('/Proveedores/listar/update');
	}

}
