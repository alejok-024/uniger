<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservas extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			//Validación de sesión
			if(!$this->session->userdata('logged_in'))
			{
				redirect('/');//Si no hay variable de sesión activa
			}
			else
			{
				$this->load->model('Reservas_Model');//Se instancia el modelo
			}
	}
	
	public function index()
	{
		//echo '<script>alert('.$this->session->userdata('Usu_Rol').');</script>';

		if ($this->session->userdata('Usu_Rol') == 0){
			
			$this->load->view('pages-calendario-reservas');
		}
		else {

			$this->load->view('pages-calendario-reservas-usuario');
		}
		
	}

	public function crear_reserva()
	{
			
		//Seteo las variables
		$datosReserva = array
		(
			'Res_Cantidad_Personas' => $this->input->post('Res_Cantidad_Personas') ,
			'Res_Estado' => $this->input->post('Res_Estado'),
			'Res_Tipo_Evento' => $this->input->post('Res_Tipo_Evento'),
			'Res_Fecha_Inicio' => $this->input->post('Res_Fecha_Inicio'),
			'Res_Fecha_Fin' => $this->input->post('Res_Fecha_Fin'),
			'Zon_Com_Id' => 1,
			'Token' => $this->input->post('Res_Token')
		);

		$result = $this->Reservas_Model->crear_reserva($datosReserva);

		if ($result):
			echo json_encode($this->input->post());
		endif;
		
    }
	
	public function Find_Reserva()
	{
		$Reserva_token = $this->input->post('Res_Token');

		$datos['zonacomun']=$this->Reservas_Model->Find_reserva($Reserva_token);
		
		if ($datos['zonacomun']):
			echo json_encode($datos['zonacomun']);
		endif;

	}

	public function Editar_Reserva(){

		$Token = $this->input->post('Res_Token');

		$datosReserva = array
		(
            'Res_Cantidad_Personas' => $this->input->post('Res_Cantidad_Personas') ,
			'Res_Estado' => $this->input->post('Res_Estado'),
			'Res_Tipo_Evento' => $this->input->post('Res_Tipo_Evento')
		);

		$result = $this->Reservas_Model->Editar_Reserva($datosReserva, $Token);

		if ($result):
			echo json_encode($this->input->post());
		endif;
	}

	public function Delete_Reserva(){

		$Token = $this->input->post('Res_Token');
		$result = $this->Reservas_Model->Delete_Reserva($Token);

        if ($result):
			echo json_encode($this->input->post());
		endif;
	}
	
	public function FindAll_Reserva()
	{
		
		$datos['zonacomun']=$this->Reservas_Model->FindAll_reserva();
		
		// echo '<script>'.json_encode($datos['zonacomun']).'</script>';

		if ($datos['zonacomun']):
			echo json_encode($datos['zonacomun']);
		endif;

	}
}