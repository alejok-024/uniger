<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Security extends CI_Controller {

    public function __construct()
	{
			parent::__construct();

                $this->load->Model('Autenticacion_Model');//Se instancia el modelo para autenticación
                $this->load->Model('Usuarios_Model');
	}
    
    /**
     * Recibe y valida credenciales de acceso, además setea variables de sesion o devuelve el usuario al inicio
     * @param N/A
     * @return N/A
     */
	public function login()
	{	     
            //Seteo las validaciones
            $this->form_validation->set_rules('email', 'Usuario', 'required|valid_email');
            $this->form_validation->set_rules('contrasena', 'Contraseña', 'required');
            //Hago la validación
            if ($this->form_validation->run() == FALSE)
            {
                redirect('/?success=no');
            }
            else
            {
                //Se Reciben Variables
                $email =  $this->security->xss_clean($this->input->post('email'));
                $contrasena =  $this->security->xss_clean($this->input->post('contrasena'));
                
                //Se validan las credenciales
                if($this->Autenticacion_Model->login($email, $contrasena)){
                    $this->load->model('Usuarios_Model');
                    $datosPerfil = $this->Usuarios_Model->validarSiPerfilEstaCompleto($email, $contrasena);

                    //Validación de si los datos fueron completados
                    if ($datosPerfil->Usu_Tipo_Documento == null 
                    or $datosPerfil->Usu_Numero_Documento == null 
                    or $datosPerfil->Usu_Fecha_Nacimiento == null)
                    {
                        $this->session->set_flashdata('datos', $datosPerfil);
                        $this->session->set_flashdata('incompleto', true);
                        redirect('Inicio/');//Esto se debe cambiar por el inicio
                    }       
                    else
                    {
                        redirect('Inicio/');//Esto se debe cambiar por el inicio
                    }             
                }
                else{
                    redirect('/?success=no');//Envia el usuario al inicio con una variable para mostrar el error de autenticación
                }
            }

    }

    
    /**
     * Recibe, valida y guarda los datos para el registro de un usuario
     * @param N/A
     * @return N/A
     */
	public function registro()
	{	    
            //Seteo las validaciones
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>', '</div>');
            $this->form_validation->set_rules('Usu_Email', 'Email', 'required|valid_email|is_unique[Usuarios.Usu_Email]');
            $this->form_validation->set_rules('Usu_Nombres', 'Nombres', 'required|trim|callback_alpha_dash_space');
            $this->form_validation->set_rules('Usu_Apellidos', 'Apellidos', 'required|trim|callback_alpha_dash_space');
            $this->form_validation->set_rules('Usu_Contrasena', 'Contraseña', 'required|min_length[6]');
            $this->form_validation->set_rules('Usu_ReContrasena', 'Contraseña', 'required|matches[Usu_Contrasena]');
            $this->form_validation->set_rules('Usu_Apto', 'Apartamento', 'required|integer|min_length[2]|max_length[3]');
            $this->form_validation->set_rules('Usu_Comentario', 'Comentarios', 'required');
            $this->form_validation->set_rules('Usu_Telefono', 'Teléfono', 'required|integer|min_length[7]');
            //Hago la validación
            if ($this->form_validation->run() == FALSE)
            {
                $this->load->view('pages-registro');
            }
            else
            {
                //Se Reciben Variables para tabla Usuario
                $datosUsu = array
                (
                    'Usu_Nombres' =>  $this->security->xss_clean($this->input->post('Usu_Nombres')),
                    'Usu_Apellidos' =>  $this->security->xss_clean($this->input->post('Usu_Apellidos')),
                    'Usu_Telefono_Celular' =>  $this->security->xss_clean($this->input->post('Usu_Telefono')),
                    'Usu_Email'  =>  $this->security->xss_clean($this->input->post('Usu_Email')),
                    'Usu_Apto' => $this->security->xss_clean($this->input->post('Usu_Comentario')),
                    'Usu_Contrasena' =>  sha1(md5($this->security->xss_clean($this->input->post('Usu_Contrasena')))),
                    'Usu_Comentario' =>  $this->security->xss_clean($this->input->post('Usu_Apto')),
                    'Usu_Estado' =>  'Pendiente'
                );

                //Se Reciben Variables para tabla Registro_Detalle
                $datosLog = array
                (
                    'Reg_Nombres' =>  $this->security->xss_clean($this->input->post('Usu_Nombres')),
                    'Reg_Apellidos' =>  $this->security->xss_clean($this->input->post('Usu_Apellidos')),
                    'Reg_Telefono' =>  $this->security->xss_clean($this->input->post('Usu_Telefono')),
                    'Reg_Email'  =>  $this->security->xss_clean($this->input->post('Usu_Email')),
                    'Reg_Apto' => $this->security->xss_clean($this->input->post('Usu_Comentario')),
                    'Reg_Contrasena' =>  sha1(md5($this->security->xss_clean($this->input->post('Usu_Contrasena')))),
                    'Reg_Comentario' =>  $this->security->xss_clean($this->input->post('Usu_Apto')),
                    'Reg_Estado' =>  'Pendiente'
                );
                
                $this->Usuarios_Model->registarUsuario($datosUsu);
                $this->Usuarios_Model->registarLogUsuario($datosLog);
                redirect('registro?success=yes');
            }
    }

    /**
     * Valida que el email exista para proceder con la generación del token para recuperación de contraseña
     * @param N/A
     * @return N/A
     */
	public function validarEmailRecuperacionContrasena()
	{	    
        //Seteo de reglas de validación
        $this->form_validation->set_rules('Usu_Email', 'Email', 'required|is_unique[Usuarios.Usu_Email]');

        //Hago la validación
        if ($this->form_validation->run() == TRUE)
        {
            $this->session->set_flashdata('error', error_display());
        }
        else
        {
            self::generarTokenContrasena($this->input->post('Usu_Email'));
        }
    }
    /**
     * Método que genera un token y envia el correo al usuario para la recuperación de su contraseña
     * @param N/A
     * @return N/A
     */
	public function generarTokenContrasena($Usu_Email)
	{	    
        #se genera una cadena de texto aleatoria
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$charactersLength = strlen($characters);
    	$cadena_texto = '';
    	for ($i = 0; $i < 10 ; $i++) {
	        $cadena_texto .= $characters[rand(0, $charactersLength - 1)];
	    }
		#se guarda en base de datos la solicitud de cambio de contraseña con una cadena de texto aleatoria
        $datos = array(
            'Reg_Con_Email' => $Usu_Email,
            'Reg_Con_Token' => $cadena_texto,
            'Reg_Con_Estado' => '0'
        );
	    $this->Autenticacion_Model->guardarToken($datos);
		#se envia correo, en el link debe estar como variables get el correo y la cadena de texto aleatoria
	
		$rute=base_url()."Gestion/index/".$datos['Reg_Con_Email']."/".$cadena_texto;
		$contenidoEditado="''
			<h5>¡Hola!<p>

			<p>Hemos recibido su solicitud para recuperar la contraseña</p>

			<p>por favor use el siguiente link para restablecer su contraseña</p>

			<a href='".$rute."' target='_blank'>Restablecer contraseña</a>

            <p>Este link es válido durante las próximas 6 horas</p>

			<p>Gracias!</p>
		";

		$config['protocol']    = 'smtp'; //Protocolo a través del cual se enviará el email
		$config['smtp_host']    = 'ssl://a2plcpnl0562.prod.iad2.secureserver.net'; //Servidor
		$config['smtp_port']    = '465'; //Puerto
		$config['smtp_timeout'] = '12'; //Tiempo de espera
		$config['smtp_user']    = 'noreply@bgvaequilibrium.com'; //Usuario que enviará el email
		$config['smtp_pass']    = '123qwe..'; //Contraseña de usuario
		$config['charset']    = 'utf-8'; //Códificación 
		$config['newline']    = "\r\n";
		$config['mailtype'] = 'html'; // Tipo de email "text" o "html"
		$config['validation'] = TRUE; // Validación de si se hará autenticación


		$this->load->library('email', $config); //Carga la librería
		$this->email->initialize($config); //Inicializa la librería y setea la configuración a usar
		$this->email->set_newline("\r\n");
		$this->email->from('noreply@bgvaequilibrium.com	', 'Uniger'); //Cómo el usuario verá quien le envió el email
		$this->email->to($datos['Reg_Con_Email']); //Email al que se enviará el correo
		$this->email->subject('Restablecer contraseña Uniger'); //Asunto del email
		$this->email->message($contenidoEditado); //Contenido del mensaje
		if (!$this->email->send()){ 
			echo $this->email->print_debugger(); 
		}else {
            $this->session->set_flashdata('envioMail', true);
            redirect('/');
		}
    }


    /**
     * Método para validar si una cadena tiene formato de fecha
     * @param N/A
     * @return N/A
     */
    public function alpha_dash_space($str)
	{
        if( ! preg_match("/^([-a-z_ ])+$/i", $str))
        {
            $this->form_validation->set_message('alpha_dash_space', 'El campo {field} no puede contener números.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
	}

    /**
     * Método para cerrar sesión
     * @param N/A
     * @return N/A
     */
    public function salir()
	{	     
        //Destruye la sesión
        $this->session->sess_destroy();
        redirect('/');
	}

}
