<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validation extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        //Validación de sesión
        $this->load->model('Inmuebles_Model');//Se instancia el modelo para muebles
	}
	public function index()
	{
		$this->load->view('pages-inicio-inmuebles');
	}

	public function validarInmueblePorId($id)
	{
		$result = $this->Inmuebles_Model->validarInmueblePorId($id);
		echo $result;
	}
}
