<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			$this->load->model('Inmuebles_Model');
	}
	public function index()
	{
		$this->load->view('pages-ingreso');
	}
	public function crear()
	{
		$datos['usuarios'] = $this->Inmuebles_Models->listar_usuarios();
		$this->load->view('pages-crear-inmuebles', $datos);
	}
	public function crear_inmueble()
	{
		$datosInmueble = array
		(
			'Inm_Tipo' => $this->input->post('Inm_Tipo') ,
			'Inm_Direccion' => $this->input->post('Inm_Direccion'),
			'Inm_Numero_Matricula' => $this->input->post('Inm_Numero_Matricula'),
			'Inm_Area_Construida' => $this->input->post('Inm_Area_Construida'),
			'Inm_Area_Privada' => $this->input->post('Inm_Area_Privada'),
			'Inm_Coeficiente_Propiedad' => $this->input->post('Inm_Coeficiente_Propiedad')
		);

		$datosDueno = array
		(
			'Usu_Id' => $this->input->post('Usu_Id'),
		);

		$result = $this->Inmuebles_Models->crear_inmueble($datosInmueble, $this->input->post('Usu_Id'));
		/*$this->Inmuebles_Models->crear_usu_inmueble($datosDueno);*/

		if($result):
			redirect('/');
		else:
			echo "no guardó";
		endif;

	}
	public function borrar_inmueble($id){
		$this->Inmuebles_Models->borrar_inmueble($id);
		redirect("/");
	}
	public function editar_inmueble($id){
		$datos['informacion'] = $this->Inmuebles_Models->listar_inmueblesxId($id);
		$datos['usuarios'] = $this->Inmuebles_Models->listar_usuarios();
		$datos['id']= $id;
		if($datos['informacion']){
			$this->load->view('pages-actualizar-inmuebles', $datos);
		}else{
			echo "No se encontraron resultados";
		}
	}

	public function actualizar_inmueble(){

		$inmueble_id = $this->input->post('inmueble_id');
		$datosInmueble = array
		(
			'Inm_Tipo' => $this->input->post('Inm_Tipo') ,
			'Inm_Direccion' => $this->input->post('Inm_Direccion'),
			'Inm_Numero_Matricula' => $this->input->post('Inm_Numero_Matricula'),
			'Inm_Area_Construida' => $this->input->post('Inm_Area_Construida'),
			'Inm_Area_Privada' => $this->input->post('Inm_Area_Privada'),
			'Inm_Coeficiente_Propiedad' => $this->input->post('Inm_Coeficiente_Propiedad')
		);

		$datosDueno = array
		(
			'Usu_Id' => $this->input->post('Usu_Id'),
		);

		$this->Inmuebles_Models->actualizar_inmueble($datosInmueble, $inmueble_id);
		$this->Inmuebles_Models->actualizar_usu_inmueble($datosDueno, $inmueble_id );

		redirect('/');
	}
}
