<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ZonaComun extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			//Validación de sesión
			if(!$this->session->userdata('logged_in'))
			{
				redirect('/');//Si no hay variable de sesión activa
			}
			else
			{
				$this->load->model('ZonaComun_Model');//Se instancia el modelo
			}
	}
	public function index()
	{
		$this->load->view('pages-zonacomun');
	}
	public function listar()
	{
		$datos['zonacomun']=$this->ZonaComun_Model->listar_zonacomun();
		$this->load->view('pages-consultar-zonacomun', $datos);
	}
	public function crear()
	{
		$datos['zonascomunes'] = $this->ZonaComun_Model->listar_zonacomun();
		$this->load->view('pages-registrar-zonacomun', $datos);
	}
	public function crear_zonacomun()
	{
		//setear validaciones
		$this->form_validation->set_error_delimiters("<div style='color: red;'>", "</div>");
		$this->form_validation->set_rules('Zon_Com_Nombre', 'Nombre de Zona', 'required|alpha_numeric_spaces');
		$this->form_validation->set_rules('Zon_Periodo_Mantenimiento', 'Periodo de Mantenimiento', 'required|numeric');
		$this->form_validation->set_rules('Zon_Descripcion', 'Descripcion', 'required');

		//hacer validacion
		if ($this->form_validation->run() == FALSE)
		{
				$this->load->view('pages-registrar-zonacomun');
		}

		else 
		{
			//Seteo las variables
			$datosZonacomun = array
			(
				'Zon_Com_Nombre' => $this->input->post('Zon_Com_Nombre'),
				'Zon_Periodo_Mantenimiento' => $this->input->post('Zon_Periodo_Mantenimiento'),
				'Zon_Descripcion' => $this->input->post('Zon_Descripcion')
            );

            $result = $this->ZonaComun_Model->crear_zonacomun($datosZonacomun);

            if ($result):
                 redirect('/ZonaComun/listar/success');
            endif;
        }
    }

	public function borrar_zonacomun(){
		

		$Token = $this->input->post('Res_Token');
		$result = $this->ZonaComun_Model->borrar_zonacomun($Token);

		if ($result):
			echo $result;
		endif;


        //redirect('/ZonaComun/listar/delete');
        
	}
	public function editar_zonacomun($id){
		$datos['informacion'] = $this->ZonaComun_Model->listar_zonacomunxId($id);
		$datos['zonacomun'] = $this->ZonaComun_Model->listar_zonacomun();
		$datos['id']= $id;

		if($datos['informacion']){
			$this->load->view('pages-actualizar-zonacomun', $datos); //crear la vista
		}else{
			echo "No se encontraron resultados";
		}
	}

	public function actualizar_zonacomun(){

		$zonacomun_id = $this->input->post('zonacomun_id');

		$datosZonacomun = array
		(
            'Zon_Com_Nombre' => $this->input->post('Zon_Com_Nombre'),
            'Zon_Periodo_Mantenimiento' => $this->input->post('Zon_Periodo_Mantenimiento'),
            'Zon_Descripcion' => $this->input->post('Zon_Descripcion')
		);

		$this->ZonaComun_Model->actualizar_zonacomun($datosZonacomun, $zonacomun_id);

		redirect('/ZonaComun/listar/update');
	}

	public function Buscar() {

		$this->form_validation->set_rules('Zon_Com_Nombre', 'Nombre de Zona', 'alpha_numeric_spaces');
		$this->form_validation->set_rules('Zon_Periodo_Mantenimiento', 'Periodo de Mantenimiento', 'numeric');
		//$this->form_validation->set_rules('Zon_Descripcion', 'Descripcion', 'alpha_numeric_spaces');

		if ($this->form_validation->run() == FALSE)
		{
				$this->load->view('pages-consultar-zonacomun');
		}else{

		$BuscarDatos = array
		(
			'Zon_Com_Nombre' => $this->input->post('Zon_Com_Nombre'),
			'Zon_Periodo_Mantenimiento' => $this->input->post('Zon_Periodo_Mantenimiento'),
			//'Zon_Descripcion'=> $this ->input->post('Zon_Descripcion')
		);

		$datos['zonacomun']=$this->ZonaComun_Model->Buscar($BuscarDatos);

		$this->load->view('pages-consultar-zonacomun', $datos);
	}
}
	public function reportes() {

		$datos['reportes']=$this->ZonaComun_Model->Reportes();
		//$datos['reportesdos']=$this->ZonaComun_Model->ReportesDos();
		$datos['reportestres']=$this->ZonaComun_Model->ReportesTres();
		$this->load->view('pages-zonacomun-reportes', $datos);

	}

}
