<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3e5f76c088e61495625383c211fbc190
{
    public static $prefixLengthsPsr4 = array (
        'K' => 
        array (
            'Konekt\\PdfInvoice\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Konekt\\PdfInvoice\\' => 
        array (
            0 => __DIR__ . '/..' . '/konekt/pdf-invoice/src',
        ),
    );

    public static $classMap = array (
        'FPDF' => __DIR__ . '/..' . '/setasign/fpdf/fpdf.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3e5f76c088e61495625383c211fbc190::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3e5f76c088e61495625383c211fbc190::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit3e5f76c088e61495625383c211fbc190::$classMap;

        }, null, ClassLoader::class);
    }
}
