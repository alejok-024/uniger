<?php
  $lang['number']		= 'Referencia';
  $lang['date']			= 'Fecha';
  $lang['time']			= 'Hora';
  $lang['due']			= 'Facha de vencimiento';
  $lang['to']			= 'Facturado ';
  $lang['from']			= 'Conjunto residencial ';
  $lang['product']		= 'Resumen';
  $lang['qty']			= 'Cantidad';
  $lang['price']		= 'Valor';
  $lang['discount']		= 'Descuento';
  $lang['vat']			= 'Impuestos';
  $lang['total']		= 'Total';
  $lang['page']			= 'Página';
  $lang['page_of']		= 'de';
?>