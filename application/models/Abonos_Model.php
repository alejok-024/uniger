<?php
class Abonos_Model extends CI_Model

{
    public function traer_abonos_por_recibo($Rec_Pag_Id)
    {
        $this->db->select('Rec_Pag_Id');
        $this->db->where('Rec_Pag_Pdf = ', $Rec_Pag_Id.".pdf");
        $query = $this->db->get('Recibo_Pago');
        

        $this->db->select('a.*');
        $this->db->where('a.Rec_Pag_Id =', $query->result()[0]->Rec_Pag_Id);
        $query = $this->db->get('Abonos a');

        return $query->result();
        
    }

    public function guardar_abono($valor, $rec_pag_id)
    {
        $this->db->set('Abo_Fecha', 'CURDATE()', false);
        $this->db->set('Abo_Valor', $valor);
        $this->db->set('Rec_Pag_Id', $rec_pag_id);
        $this->db->insert('Abonos');
    }

    public function eliminar_abono($abo_id)
    {
        $this->db->where('Abo_Id', $abo_id);
        $this->db->delete('Abonos');
        
    }
}
?>
