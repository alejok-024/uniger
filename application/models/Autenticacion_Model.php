<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autenticacion_Model extends CI_Model {
    
    
    public function login($email, $contrasena){
    	$contrasena = sha1(md5($contrasena));
        $this->db->where('Usu_Email',$email);
        $this->db->where('Usu_Contrasena',$contrasena);
        $this->db->where('Usu_Estado', '1');
        $query = $this->db->get('Usuarios');
        if($query->num_rows()==1){
            foreach ($query->result() as $row){
                $data = array(
                            'Usu_Email'=> $row->Usu_Email,
                            'Usu_Contrasena'=> $row->Usu_Contrasena,
                            'Usu_Nombres'=> $row->Usu_Nombres,
                            'Usu_Rol'=> $row->Usu_Rol,
                            'Usu_Id'=> $row->Usu_Id,
                            'logged_in'=>TRUE

                        );
            }
            $this->session->set_userdata($data);
            return TRUE;
        }
        else{
            return FALSE;
      }    
    }
    
    public function isLoggedIn(){
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        $is_logged_in = $this->session->userdata('logged_in');

        if(!isset($is_logged_in) || $is_logged_in!==TRUE)
        {
            redirect('/');
            exit;
        }
    }
    
    public function guardarToken($datos){

        $this->db->set('Reg_Con_Fecha_Expira', 'DATE_ADD(NOW(), INTERVAL 1 DAY)', false);
        $this->db->insert('Recuperacion_Contrasena', $datos);
        
    }
    
    public function validarToken($correo, $token){

        $this->db->select('Reg_Con_Estado');
        $this->db->from('Recuperacion_Contrasena');
        $this->db->where('Reg_Con_Email', $correo);
        $this->db->where('Reg_Con_Token', $token);
        $this->db->where('Reg_Con_Estado !=', '1');
        $this->db->where('Reg_Con_Fecha_Expira >=', 'NOW()', false);
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return true;
        }
        else
        {
            return false;
        }

    }
    
    public function establecerContrasena($contrasena)
    {
        $contrasena = sha1(md5($contrasena));
        $this->db->set('Usu_Contrasena', $contrasena);
        $this->db->update('Usuarios');
        
        return true;
    }
    
    public function deshabilitarToken($contrasena)
    {
        $this->db->set('Reg_Con_Estado', '1');
        $this->db->update('Recuperacion_Contrasena');
        
        return true;
    }

    
    
}