<?php
class Inmuebles_Model extends CI_Model {

    public function crear_inmueble($datos, $usuario){
        
        $this->db->insert('Inmuebles', $datos);
        $datosDueno = array(
        'Usu_Id' => $usuario,
        'Inm_Id' => $datos['Inm_Id'],
        'InmXUsu_Rol' => "Dueño"
        );
        $this->db->insert('InmueblesXUsuarios', $datosDueno);


        return true;
    }

    public function listar_usuarios(){

        $this->db->select('Usu_Id, Usu_Nombres, Usu_Apellidos');
        $this->db->from('Usuarios');
        $this->db->where('Usu_Estado = 1');
        $query = $this->db->get();

        return $query->result();
    }

    public function listar_inmuebles(){

        $sql=$this->db->query("SELECT I.*, U.Usu_Nombres, Usu_Apellidos 
        FROM Inmuebles I JOIN InmueblesXUsuarios X ON I.Inm_Id = X.Inm_Id 
        JOIN Usuarios U ON U.Usu_Id = X.Usu_Id");
        return $sql->result();
    }
    public function listar_inmueblesxId($id){

        $sql=$this->db->query("SELECT I.*, U.Usu_Id, U.Usu_Nombres, Usu_Apellidos 
        FROM Inmuebles I JOIN InmueblesXUsuarios X ON I.Inm_Id = X.Inm_Id 
        JOIN Usuarios U ON U.Usu_Id = X.Usu_Id where I.Inm_Id = $id");

        return $sql->result()[0];
    }
    
    public function borrar_inmueble($id){
        $this->db->delete('Inmuebles', array('Inm_Id' => $id));
        $this->db->delete('InmueblesXUsuarios', array('Inm_Id' => $id));
        return true;
    }

    public function actualizar_inmueble($datos, $id){
        
        $this->db->set($datos);
        $this->db->where('Inm_Id', $id);
        $this->db->update('Inmuebles');

        return true;
    }
    
    public function actualizar_usu_inmueble($datos, $id){
        
        $this->db->set($datos);
        $this->db->where('Inm_Id', $id);
        $this->db->update('InmueblesXUsuarios');

        return true;
    }

    public function Buscar($BuscarDatos){
        $this->db->select('U.Usu_Nombres,U.Usu_Apellidos,I.Inm_Id,Inm_Numero_Matricula,I.Inm_Area_Construida,I.Inm_Tipo');
        $this->db->from('Inmuebles I');
        $this->db->join('InmueblesXUsuarios x','x.Inm_Id = I.Inm_Id');
        $this->db->join('Usuarios U','x.Usu_Id=U.Usu_Id');
 
        foreach ($BuscarDatos as $columna => $valor) {
        if ($valor != "") {
          if ($columna == "Usu_Nombres" || $columna == "Usu_Apellidos") {
        $this->db->like("U.".$columna, $valor);
        $columna = "I.".$columna;
          } else {
        $this->db->like("I.".$columna, $valor);
          }       
        }
        }
 
        $sql=$this->db->get(); 
        return $sql->result();
    }

    public function validarInmueblePorId($id)
    {
        $this->db->select('Inm_Id');
        $this->db->where('Inm_Id', $id);
        $result = $this->db->get('Inmuebles');

        if (COUNT($result->result()) > 0) {
         return 1;
        }else{
        return 2;
        }
    }

    /**
     * Obtiene todos los inmuebles y roles de un usuario 
     * @param type $id 
     * @return type array 
     */
    public function inmueblesPorUsuario($id)
    {
        $this->db->select("iu.`Inm_Id`, iu.`InmXUsu_Rol`, i.`Inm_Coeficiente_Propiedad`, i.`Inm_Area_Construida`");
        $this->db->from('InmueblesXUsuarios iu');
        $this->db->join('Inmuebles i', 'i.`Inm_Id` = iu.`Inm_Id`');
        ('InmueblesXUsuarios iu');
        $this->db->where('Usu_Id',  $id);
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result();
        }
        else
        {
            return false;
        }
    }

    /**
     * Obtiene todos los inmuebles y roles de un usuario 
     * @param type $id 
     * @return type array 
     */
    public function inmueblePorUsuario($inm_id, $usu_id)
    {
        $this->db->select("iu.`Inm_Id`, iu.`InmXUsu_Rol`, i.`Inm_Coeficiente_Propiedad`, i.`Inm_Area_Construida`");
        $this->db->from('InmueblesXUsuarios iu');
        $this->db->join('Inmuebles i', 'i.`Inm_Id` = iu.`Inm_Id`');
        ('InmueblesXUsuarios iu');
        $this->db->where('iu.Usu_Id',  $usu_id);
        $this->db->where('iu.Inm_Id',  $inm_id);
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result()[0];
        }
        else
        {
            return false;
        }
    }
    public function ReportesInmuebles() {

        $sql=$this->db->query("SELECT COUNT(Inm_Id) AS numInmuebles, Inm_Tipo FROM Inmuebles GROUP BY Inm_Tipo");

        return $sql->result();
    }
    public function listar_usuarios_pdf(){

        $this->db->select('Usu_Id, Usu_Nombres, Usu_Apellidos');
        $this->db->from('Usuarios');
        $query = $this->db->get();

        return $query->result();
    }
    public function listar_inmueblesxId_pdf($id){

        $sql=$this->db->query("SELECT I.*, U.Usu_Id, U.Usu_Nombres, Usu_Apellidos, InmXUsu_Rol
        FROM Inmuebles I JOIN InmueblesXUsuarios X ON I.Inm_Id = X.Inm_Id 
        JOIN Usuarios U ON U.Usu_Id = X.Usu_Id where I.Inm_Id = $id");

        return $sql->result()[0];
    }
    // Listar usuarios de un inmueble
    public function listar_usuarios_editar($id){
        $this->db->select('i.Inm_Id, u.Usu_Id, u.Usu_Nombres, u.Usu_Apellidos, i.InmXUsu_Rol');
        $this->db->from('Usuarios u');
        $this->db->join('InmueblesXUsuarios i', 'i.Usu_Id = u.Usu_Id');
        $this->db->Where('i.Inm_Id', $id);
        $query = $this->db->get();
        return $query->result();
    }
    public function listar_usuarios_Creados(){
        $this->db->select('Usu_Id, Usu_Nombres, Usu_Apellidos');
        $this->db->where('Usu_Estado = 1');
        $this->db->from('Usuarios ');
        
        $query = $this->db->get();
        return $query->result();
    }
    public function editar_usu(){
        
        $sql=$this->db->query("SELECT I.*, U.Usu_Id, U.Usu_Nombres, Usu_Apellidos 
        FROM Inmuebles I JOIN InmueblesXUsuarios X ON I.Inm_Id = X.Inm_Id 
        JOIN Usuarios U ON U.Usu_Id = X.Usu_Id where I.Inm_Id = $id");
        return $sql->result()[0];
    }
    public function borrar_usuario($usu_id, $inm_id, $rol){

        $this->db->where('Usu_Id', $usu_id);
        $this->db->where('InmXUsu_Rol', $rol);
        $this->db->where('Inm_Id', $inm_id);
        $this->db->delete('InmueblesXUsuarios');
        echo $this->db->last_query();
    }
    //Agregar usuario a un inmueble
    public function agregar(){
        $this->db->select('Usu_Id, Usu_Nombres, Usu_Apellidos');
        $this->db->from('Usuarios');
        $query = $this->db->get();

        return $query->result();
    }

    public function agregar_usu($datos){
            
        $this->db->insert('InmueblesXUsuarios', $datos);

        return true;
    
    }
    public function Actulizar_Usu($datos){
        $this->db->set('InmXUsu_Rol',$datos['InmXUsu_Rol']);
        $this->db->where('Usu_Id',$datos['Usu_Id']);
        $this->db->where('Inm_Id',$datos['Inm_Id']);
        $this->db->update('InmueblesXUsuarios');

        return true;
    }

}

?>