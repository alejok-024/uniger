<?php
class Mantenimiento_Model extends CI_Model {

    public function crear_mantenimiento($datos){
        

        $this->db->insert('Mantenimiento', $datos);

        return true;
    }

    public function listar_mantenimiento(){

        $this->db->select('Man_Id, Man_Tipo, Man_Fecha, Man_Tiempo, Man_Comentario_Inicial, Man_Comentario_Final, Man_Calificacion');
        $this->db->from('Mantenimiento');
        $query = $this->db->get();

        return $query->result();
    }

    public function listar_mantenimientoxId($id){

        $sql=$this->db->query("SELECT * FROM Mantenimiento WHERE Man_Id = $id");

        return $sql->result()[0];
    }
    
    public function borrar_mantenimiento($id){
        $this->db->delete('Mantenimiento', array('Man_Id' => $id));
        //return true;
    }

    public function actualizar_mantenimiento($datos, $id){
        
        $this->db->set($datos);
        $this->db->where('Man_Id', $id);
        $this->db->update('Mantenimiento');

        return true;
    }

    public function listar_zonas() {
    $this->db->select('Zon_Com_Id, Zon_Com_Nombre');
    $this->db->from('Zonas_Comunes');
    $query = $this->db->get();
    return $query->result();
}

public function listar_proveedor() {
    $this->db->select('Pro_Id, Pro_Nombre');
    $this->db->from('Proveedores');
    $query = $this->db->get();
    return $query->result();
}

}
?>