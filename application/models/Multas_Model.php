<?php
class Multas_Model extends CI_Model

{

    public function eliminar_multa($multa_id)
    {
        $this->db->where('Mul_Id', $multa_id);
        $this->db->delete('Multas');
    }

    
    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_usuarios_registrados()
    {

        $this->db->select('Usu_Id, CONCAT(Usu_Nombres," ",Usu_Apellidos ) AS Nombres ');    
        $this->db->where('Usu_Estado =', 1);
        $query = $this->db->get('Usuarios');

        return $query->result();
    }

    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_inmuebles()
    {

        $this->db->select('Inm_Id');    
        $query = $this->db->get('Inmuebles');

        return $query->result();
    }
    
    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_meses_disponibles()
    {

        $this->db->select('*');    
        $this->db->where('Mes_Id >', 'MONTH(CURDATE())', false);
        
        $query = $this->db->get('Mes');

        return $query->result();
    }

    /**
     * Consulta los periodos mayores o iguales al periodo actual
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function traer_periodos()
    {
    
        $this->db->select('Per_Periodo');
        $this->db->where('Per_Periodo >=', 'YEAR(CURDATE())', false);
        $query = $this->db->get('Periodo');

        return $query->result();
    }

    /**
     * Guarda el nuevo valor para ser recaudadopor concepto de cuota extraordinaria
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function guardar_multa($datos)
    {   
        $this->db->insert('Multas', $datos); 
    }

    /**
     * Crea los detalles del cobro de cuota extraordinaria ques erá realizado por inmueble
     *
     * @param       array  $datos
     * @return      N/A
     */
    function guardar_detalle_cuota_extraordinaria($valor, $id_presupuesto, $mes, $descripcion)
    {
        $inmuebles = $this->inm_traer_datos_presupuesto();
        foreach ($inmuebles as $inmueble) 
        {
            $total_pagar = round($valor*$inmueble->Inm_Coeficiente_Propiedad);
            
            $datos = array (
                'Cuo_Ext_Id' => $id_presupuesto,
                'Inm_Id' => $inmueble->Inm_Id,
                'Mes_Id' => $mes,
                'Det_Cuo_Ext_Valor' => $total_pagar,
                'Det_Cuo_Ext_Descripcion' => $descripcion
            );

            $this->db->insert('Detalle_Cuotas_Extraordinarias', $datos);
        }        
    }

        
    /**
     * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
     * @param       N/A
     * @return      array/object  $query->result()
	 */
    function inm_traer_datos_presupuesto()
    {
        
        $this->db->select('Inm_Id, Inm_Coeficiente_Propiedad');
        $query = $this->db->get('Inmuebles');
        return $query->result();
    }

    
    public function traer_detalles_cuotas_extraordinarias()
    {
        $this->db->select('d.*, i.Inm_Tipo, c.Per_Periodo, m.Mes_Nombre');
        $this->db->join('Inmuebles i', 'd.Inm_Id = i.Inm_Id', 'left');
        $this->db->join('Mes m', 'd.Mes_Id = m.Mes_Id', 'left');
        $this->db->join('Cuotas_Extraordinarias c', 'd.Cuo_Ext_Id = c.Cuo_Ext_Id', 'left');
        $this->db->group_by('Per_Periodo, Inm_Id');
        $query = $this->db->get('Detalle_Cuotas_Extraordinarias d');
 
        return $query->result();
    }
    
    public function traer_multas()
    {
        $this->db->select('mu.*, m.Mes_Nombre');
        $this->db->join('Mes m', 'mu.Mes_Id = m.Mes_Id', 'left');
        $query = $this->db->get('Multas mu');
        
        return $query->result();
    }
    
    public function crear_recibos_pago($mes_anterior, $inmuebleProcesar, $periodo_actual, $recibo_id)
    {
        echo $mes_anterior, $inmuebleProcesar."<br>";
        echo $periodo_actual."<br>"; 
        echo $recibo_id."<br>";
        $this->db->select('Mul_Id');    
        $this->db->where('Mes_Id', $mes_anterior);
        $this->db->where('Inm_Id', $inmuebleProcesar);
        $this->db->where('Per_Periodo', $periodo_actual);
        $query = $this->db->get('Multas');

        foreach($query->result() as $row)
        {
            $datos = array(
                'Mul_Id' => $row->Mul_Id,
                'Det_Rec_Pag_Estado' => 'Por Imprimir',
                'Rec_Pag_Id' => $recibo_id
            );

            $this->db->insert('Detalle_Recibo_Pago', $datos);
            
        }
    }

    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_multas_por_recibo($Rec_Pag_Id)
    {
        
        $this->db->select('Rec_Pag_Id');
        $this->db->where('Rec_Pag_Pdf = ', $Rec_Pag_Id.".pdf");
        $query = $this->db->get('Recibo_Pago');
        
        $this->db->select('m.Mul_Valor');
        $this->db->join('Multas AS m', 'd.`Mul_Id` = m.`Mul_Id`');
        $this->db->join('Recibo_Pago AS rp', 'd.`Rec_Pag_Id` = rp.`Rec_Pag_Id`');
        $this->db->where('d.`Rec_Pag_Id` =', $query->result()[0]->Rec_Pag_Id);
        
        $query = $this->db->get('Detalle_Recibo_Pago AS d');
        
        return $query->result();
    }

    public function reportes_multas()
    {
        $this->db->select('SUM(Mul_valor) Mul_valor, Mes_Id');
        $this->db->from('Multas');
        $this->db->Where('Per_Periodo','YEAR(curdate())', FALSE);
        $this->db->group_by('Mes_Id');
        $query = $this->db->get();
        return $query->result();

    }

    public function multas_inmuebles()
    {
        $this->db->select('SUM(Mul_Valor) Mul_Valor, Inm_Id');
        $this->db->from('Multas');
        $this->db->group_by('Inm_Id');
        $query = $this->db->get();
        return $query->result();
    }

    public function reportes_promedio()
    {
        $this->db->select('AVG(Mul_valor) Mul_valor, Mes_Id');
        $this->db->from('Multas');
        $this->db->Where('Per_Periodo','YEAR(curdate())', FALSE);
        $this->db->group_by('Mes_Id');
        $query = $this->db->get();
        return $query->result();

    }
}

?>
