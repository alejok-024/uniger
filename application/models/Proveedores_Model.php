<?php
class Proveedores_Model extends CI_Model {

    public function crear_proveedor($datos){
        

        $this->db->insert('Proveedores', $datos);

        return true;
    }

    public function listar_proveedor(){

        $this->db->select('Pro_Id, Pro_Tipo, Pro_Nombre, Pro_Telefono, Pro_Direccion, Pro_Correo, Pro_Contacto, Pro_Descripcion');
        $this->db->from('Proveedores');
        $query = $this->db->get();

        return $query->result();
    }

    public function listar_proveedorxId($id){

        $sql=$this->db->query("SELECT * FROM Proveedores WHERE Pro_Id = $id");

        return $sql->result()[0];
    }
    
    public function borrar_proveedor($id){
        $this->db->delete('Proveedores', array('Pro_Id' => $id));
        //return true;
    }

    public function actualizar_proveedor($datos, $id){
        
        $this->db->set($datos);
        $this->db->where('Pro_Id', $id);
        $this->db->update('Proveedores');

        return true;
    }
}
?>