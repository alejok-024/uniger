<?php
class Recibo_Model extends CI_Model

{
    public function listar_Usuarios()
    {
    $this->db->select('Usu_Id, Usu_Nombres, Usu_Apellidos');
    $this->db->from('Usuarios');
    $query = $this->db->get();
    return $query->result();
    }

    public function listar_Inmuebles()
    {
        $this->db->select('Inm_Id');
        $this->db->from('Inmuebles');
        $query = $this->db->get();
        return $query->result();
    }

    public function crear_Recibos($datosRecibo, $usuario)
    {
        $this->db->set('Reg_Pag_Fecha', 'CURDATE()', false);
        $this->db->insert('Recibo_Pago', $datosRecibo);
        return true;
    }

    public function Verificar_DB_Model($Inm_Id)
    {
    $this->db->Select('Inm_Id');
    $this->db->From('Inmuebles');
    $this->db->Where('Inm_Id', $Inm_Id);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
        {
        return false;
        }
        else
        {
        return true;
        }
    }

    public function listar_Recibos()
    {
    $this->db->select('r.Rec_Pag_Id, r.Rec_Pag_Fecha_Expedicion, r.Rec_Pag_Estado,
                    r.Rec_Pag_Fecha_Vencimiento, r.Rec_Pag_Valor, u.Usu_Nombres, u.Usu_Apellidos,
                    i.Inm_Id');
    $this->db->From('Recibo_Pago r');
    $this->db->Join('Inmuebles i', 'r.Inm_id = i.Inm_id');
    $this->db->Join('InmueblesXUsuarios x', 'x.Inm_id = i.Inm_id');
    $this->db->Join('Usuarios u', 'x.Usu_Id = u.Usu_Id');
    $this->db->where('r.Rec_Pag_Estado', NULL);
    $query = $this->db->get();
    return $query->result();
    }

    public function Validar_Recibos($Id)
    {
    $this->db->select('r.Rec_Pag_Id, r.Rec_Pag_Fecha_Expedicion,
                    r.Rec_Pag_Fecha_Vencimiento, r.Rec_Pag_Valor, r.Rec_Pag_Estado,
                    u.Usu_Nombres, u.Usu_Apellidos, i.Inm_Id');
    $this->db->From('Recibo_Pago r');
    $this->db->Join('Inmuebles i', 'r.Inm_id = i.Inm_id');
    $this->db->Join('InmueblesXUsuarios x', 'x.Inm_id = i.Inm_id');
    $this->db->Join('Usuarios u', 'x.Usu_Id = u.Usu_Id');
    $this->db->Where('r.Rec_Pag_Id', $Id);
    $query = $this->db->get();
    return $query->result();
    }

    public function cargar_Select($Id)
    {
    $this->db->select('SUM(Mul_Valor) as valor');
    $this->db->From('Multas');
    $this->db->Where('Inm_Id', $Id);
    $query = $this->db->get();
    return $query->result() [0]->valor;;
    }

    public function borrar_Recibo($Id)
    {
    $Cambiar = $this->Validar_Recibos($Id);
    $this->db->set('Rec_Pag_Estado', 'Anulado');
    $this->db->Where('Rec_Pag_Id', $Id);
    $this->db->Update('Recibo_Pago');
    $query = $this->db->affected_rows();
    return $query;
    }

    public function Abonos($Id)
    {
    $this->db->select('r.Rec_Pag_Id, r.Rec_Pag_Fecha_Expedicion,
r.Rec_Pag_Fecha_Vencimiento, r.Rec_Pag_Valor,
u.Usu_Nombres, u.Usu_Apellidos, i.Inm_Id');
    $this->db->From('Recibo_Pago r');
    $this->db->Join('Inmuebles i', 'r.Inm_id = i.Inm_id');
    $this->db->Join('InmueblesXUsuarios x', 'x.Inm_id = i.Inm_id');
    $this->db->Join('Usuarios u', 'x.Usu_Id = u.Usu_Id');
    $this->db->Where('r.Rec_Pag_Id', $Id);
    $query = $this->db->get();
    return $query->result() [0];
    }

    public function obtenerHistorialAbonos($id)
    {
    $this->db->select('*');
    $this->db->where('Rec_Pag_Id', $id);
    $query = $this->db->get('Abonos_Pagos');
    return $query->result();
    }

    public function crearAbonos($datosAbonos)
    {
    $this->db->insert('Abonos_Pagos', $datosAbonos);
    return true;
    }

    public function valorTotal($id)
    {
    $this->db->select('*');
    $this->db->From('Recibo_Pago');
    $this->db->Where('Rec_Pag_Id', $id);
    $query = $this->db->get();
    return $query->result();
    }

    public function Diferencia_Abono($id)
    {
    $this->db->select('a.*, r.Rec_Pag_Valor');
    $this->db->From('Abonos_Pagos a');
    $this->db->Join('Recibo_Pago r', 'a.Rec_Pag_Id = r.Rec_Pag_Id', 'left');
    $this->db->Where('r.Rec_Pag_Id', $id);
    $query = $this->db->get();
    return $query->result();
    }

    public function BuscarRecibos($BuscarRecibos)
    {
    $this->db->select('r.Rec_Pag_Id, r.Rec_Pag_Fecha_Expedicion, r.Rec_Pag_Estado,
                    r.Rec_Pag_Fecha_Vencimiento, r.Rec_Pag_Valor, u.Usu_Nombres, u.Usu_Apellidos,
                    i.Inm_Id');
    $this->db->From('Recibo_Pago r');
    $this->db->Join('Inmuebles i', 'r.Inm_id = i.Inm_id');
    $this->db->Join('InmueblesXUsuarios x', 'x.Inm_id = i.Inm_id');
    $this->db->Join('Usuarios u', 'x.Usu_Id = u.Usu_Id');
    foreach($BuscarRecibos as $columna => $valor)
        {
        if ($valor != "")
            {
            if ($columna == "Usu_Nombres" || $columna == "Usu_Apellidos")
                {
                $this->db->like("U." . $columna, $valor);
                $columna = "i." . $columna;
                }
                else
            if ($columna == "Inm_Id")
                {
                $this->db->like("i." . $columna, $valor);
                }
                else
                {
                $this->db->like("r." . $columna, $valor);
                }
            }
        }

    $this->db->Where('r.Rec_Pag_Estado', NULL);
    $sql = $this->db->get();
    return $sql->result();
    }

    public function ReportesFacturas()
    {
    $this->db->select('COUNT(Rec_Pag_Fecha_Expedicion) fecha, Month(Rec_Pag_Fecha_Expedicion) mes');
    $this->db->From('Recibo_Pago');
    $this->db->group_by('Month(Rec_Pag_Fecha_Expedicion)');
    $sql = $this->db->get();
    return $sql->result();
    }

    public function PDF_Recibos($id)
    {
    $this->db->select('r.Rec_Pag_Id, r.Rec_Pag_Fecha_Expedicion, r.Rec_Pag_Estado,
                        r.Rec_Pag_Extraordinaria, r.Rec_Pag_Administracion,
                    r.Rec_Pag_Fecha_Vencimiento, r.Rec_Pag_Valor,  u.Usu_Nombres,
                    u.Usu_Apellidos, i.Inm_Id');
    $this->db->From('Recibo_Pago r');
    $this->db->Join('Inmuebles i', 'r.Inm_id = i.Inm_id');
    $this->db->Join('InmueblesXUsuarios x', 'x.Inm_id = i.Inm_id');
    $this->db->Join('Usuarios u', 'x.Usu_Id = u.Usu_Id');
    $this->db->where('r.Rec_Pag_Id', $id);
    $query = $this->db->get();
    return $query->result() [0];
    }

    public function Pagar_Multas($id)
    {
    $this->db->select('SUM(Mul_Valor) as valor');
    $this->db->From('Multas');
    $query = $this->db->get();
    return $query->result() [0]->valor;
    }

    public function crear_Multas($registrarMultas)
    {
    $this->db->insert('Multas', $registrarMultas);
    return true;
    }

    public function listar_Multas()
    {
    $this->db->select('Mul_Id, Mul_Valor, Mul_DescripciÃ³n, Mul_Fecha, Inm_Id');
    $this->db->From('Multas ');
    $query = $this->db->get();
    return $query->result();
    }

    public function borrar_multa($id)
    {
    $this->db->delete('Multas', array(
        'Mul_Id' => $id
    ));
    return true;
    }

	/****************/
	/*              */
	/* Configuracion*/
	/*              */
	/****************/

    /**
	 * Crea un nuevo periodo de facturaciÃ³n para recibos de pago
	 *
	 * @param       N/A
	 * @return      N/A
	 */
    public function conf_nuevo_periodo()
    {

		// Establece el periodo para el aÃ±o en curso y deshabilita el del aÃ±o anterior
		// Query 1

		$this->db->set('Per_Estado', 0);
		$this->db->where('Per_Periodo', 'YEAR(CURDATE())', false);
		$this->db->update('periodo');

		// Se ingresa el periodo consultado + 1
		// Query 2
		// Se consulta el Ãºltimo periodo registrado

		$this->db->select_max('Per_Periodo');
		$maxPeriodo = $this->db->get('periodo');
		$maxPeriodo = $maxPeriodo->result() [0]->Per_Periodo;

		// Se inserta el nuevo periodo
		// Query 3

		$this->db->set('Per_Periodo', ($maxPeriodo + 1));
		$this->db->insert('periodo');

		// Se cambia el estado a activo para el nuevo periodo
		// Query 4

		$this->db->set('Per_Estado', 1);
		$this->db->where('Per_Periodo', 'YEAR(CURDATE())+1', false);
		$this->db->update('periodo');
    }

    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function conf_traer_periodos_sin_presupuesto()
    {

        $this->db->select('p.Per_Periodo', false);
        $this->db->from('Periodo as p');
        $this->db->join('Presupuesto AS pre', 'p.`Per_Periodo` = pre.`Per_Periodo`', 'left');
        $this->db->where('pre.`Per_Periodo` IS NULL', '', false);        
        $query = $this->db->get();

        return $query->result();
    }

    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function conf_traer_meses_disponibles()
    {

        $this->db->select('*');    
        $this->db->where('Mes_Id >', 'MONTH(CURDATE())', false);
        
        $query = $this->db->get('Mes');

        return $query->result();
    }

    /**
	 * Guarda el nuevo presupuesto general para un periodo en especifico
	 *
	 * @param       array  $datos
	 * @return      int $insert_id
	 */   
	function conf_nuevo_presupuesto($datos)
    {
    
        $this->db->insert('Presupuesto', $datos);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    /**
	 * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
	 * @param       array  $datos
	 * @return      N/A
	 */
	function conf_nuevo_detalle_presupuesto($valor, $id_presupuesto, $descripcion)
    {
        $inmuebles = $this->inm_traer_datos_presupuesto();
        foreach ($inmuebles as $inmueble) {
            $total_por_periodo_inm = round($valor*$inmueble->Inm_Coeficiente_Propiedad);
            for ($i=1; $i <= 12; $i++) { 

                $total_por_mes_inm = round($total_por_periodo_inm/12);
                
                $datos = array (
                    'Pre_Id' => $id_presupuesto,
                    'Inm_Id' => $inmueble->Inm_Id,
                    'Mes' => $i,
                    'Det_Pre_Valor' => $total_por_mes_inm,
                    'Det_Pre_Descripcion' => $descripcion
                );

                $this->db->insert('Detalle_Presupuesto', $datos);

            }
        }
        
    }
    
    /**
     * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
     * @param       N/A
     * @return      array/object  $query->result()
	 */
    function inm_traer_datos_presupuesto()
    {
        
        $this->db->select('Inm_Id, Inm_Coeficiente_Propiedad');
        $query = $this->db->get('Inmuebles');
        return $query->result();
    }
    
    /**
     * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
     * @param       N/A
     * @return      array/object  $query->result()
	 */
    function crear_recibo($nombre_pdf)
    {
        $this->db->set('Reg_Pag_Fecha', 'CURDATE()', false);
        $this->db->set('Rec_Pag_Pdf', $nombre_pdf);
        $this->db->insert('Recibo_Pago');
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }
    
    /**
     * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
     * @param       N/A
     * @return      array/object  $query->result()
	 */
    function traer_multas($mes_anterior, $inmuebleProcesar, $periodo_actual)
    {

        $this->db->select('mul.Mul_Valor, mul.Mul_Descripcion');
        $this->db->join('Multas AS mul', 'rp.Mul_Id = mul.Mul_Id', 'left');
        $this->db->where('mul.Inm_Id', $inmuebleProcesar);
        $this->db->where('mul.Mul_Tipo', 1);
        $this->db->where('mul.Per_Periodo', $periodo_actual);
        $this->db->where('mul.Mes_Id', $mes_anterior);

        $query = $this->db->get('Detalle_Recibo_Pago AS rp');
        return $query->result();
    }

    /**
     * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
     * @param       N/A
     * @return      array/object  $query->result()
	 */
    function traer_cuotas_extraordinarias($mes_anterior, $inmuebleProcesar, $periodo_actual)
    {

        $this->db->select('det.Det_Cuo_Ext_Valor, det.Det_Cuo_Ext_Descripcion');
        $this->db->join('Detalle_Cuotas_Extraordinarias AS det', 'rp.Det_Cuo_Ext_Id = det.Det_Cuo_Ext_Id', 'left');
        $this->db->join('Cuotas_Extraordinarias AS cuo', 'det.Cuo_Ext_Id = cuo.Cuo_Ext_Id', 'left');
        $this->db->where('det.Inm_Id =', $inmuebleProcesar);
        $this->db->where('cuo.Per_Periodo =', $periodo_actual);
        $this->db->where('det.Mes_Id =', $mes_anterior);

        $query = $this->db->get('Detalle_Recibo_Pago AS rp');
        return $query->result();
    }

    /**
     * Crea los detalles del cobro mes a mes ques erá realizado por inmueble
	 *
     * @param       N/A
     * @return      array/object  $query->result()
	 */
    function traer_presupuestos($mes_anterior, $inmuebleProcesar, $periodo_actual)
    {

        $this->db->select('det.Det_Pre_Valor, det.Det_Pre_Descripcion');
        $this->db->join('Detalle_Presupuesto AS det', 'rp.Det_Pre_Id = det.Det_Pre_Id', 'left');
        $this->db->join('Presupuestos AS pre', 'det.Pre_Id = pre.Pre_Id', 'left');
        $this->db->where('det.Inm_Id =', $inmuebleProcesar);
        $this->db->where('pre.Per_Periodo =', $periodo_actual);
        $this->db->where('det.Mes_Id =', $mes_anterior);

        $query = $this->db->get('Detalle_Recibo_Pago AS rp');


        return $query->result();
    }
    
    /**
     * Guarda el nuevo valor para ser recaudadopor concepto de cuota extraordinaria
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function traer_info_pdf($inmuebleProcesar)
    {

        $this->db->select("CONCAT(u.Usu_Nombres, ' ', u.Usu_Apellidos) AS nombres, i.Inm_Tipo, i.Inm_Id");
        $this->db->join('Usuarios AS u ', 'iu.`Usu_Id` = u.`Usu_Id`', 'left');
        $this->db->join('Inmuebles AS i ', 'iu.`Inm_Id` = i.`Inm_Id`', 'left');
        $this->db->where('i.`Inm_Id` =', 305);    
        $this->db->where('iu.`InmXUsu_Rol` =', "Dueño");    
        $query = $this->db->get('InmueblesXUsuarios AS iu');

        return $query->result()[0];
    }
    
    
    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_inmuebles()
    {

        $this->db->select('Inm_Id');    
        $query = $this->db->get('Inmuebles');

        return $query->result();
    }

    /**
	 *  
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_datos_pdf()
    {
        $this->db->select("Rec_Pag_Pdf, SUBSTRING_INDEX(SUBSTRING_INDEX(Rec_Pag_Pdf, '-', -1),'.',1) AS Inm_Id, SUBSTRING_INDEX(Rec_Pag_Pdf, '.', 1) AS Rec_Pag_Numero");
        
        $query = $this->db->get('Recibo_Pago AS rb');

        return $query->result();
    }


    /**
	 * Consulta los periodos registrados en base de datos que no tengan presupuesto asignado 
	 *
	 * @param       N/A
	 * @return      array/object  $query->result()
	 */   
    public function traer_periodo_actual()
    {

        $this->db->select('Per_Periodo');
        $this->db->where('Per_Estado', 1);
        $query = $this->db->get('Periodo');

        return $query->result()[0]->Per_Periodo;
    }

    /**
     * Guarda el nuevo valor para ser recaudadopor concepto de cuota extraordinaria
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function conf_nueva_cuota_extraordinaria($datos)
    {
    
        $this->db->insert('Cuota_Extraordinaria', $datos);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    /**
     * Consulta los periodos mayores o iguales al periodo actual
     *
     * @param       array  $datos
     * @return      int $insert_id
     */   
    function conf_traer_periodos()
    {
    
        $this->db->select('Per_Periodo');
        $this->db->where('Per_Periodo >=', 'YEAR(CURDATE())', false);
        $query = $this->db->get('Periodo');

        return $query->result();
    }

    /**
     * Crea los detalles del cobro de cuota extraordinaria ques erá realizado por inmueble
     *
     * @param       array  $datos
     * @return      N/A
     */
    function conf_nuevo_detalle_cuota_extraordinaria($valor, $id_presupuesto, $descripcion)
    {
        $inmuebles = $this->inm_traer_datos_presupuesto();
        foreach ($inmuebles as $inmueble) {
            for ($i=1; $i <= 12; $i++) { 

                $total_por_mes_inm = round($valor*$inmueble->Inm_Coeficiente_Propiedad);
                
                $datos = array (
                    'Cuo_Ext_Id' => $id_presupuesto,
                    'Inm_Id' => $inmueble->Inm_Id,
                    'Mes' => $i,
                    'Det_Cuo_Ext_Valor' => $total_por_mes_inm,
                    'Det_Cuo_Ext_Descripcion' => $descripcion
                );

                $this->db->insert('Detalle_Cuota_Extraordinaria', $datos);

            }
        }
        
    }

    public function filtrar_dueno()
    {
        $this->db->select('Usu_Id');
        $this->db->from('InmueblesXUsuarios');
        $this->db->where('InmXUsu_Rol','Dueño');
        $this->db->where('Usu_Id',$this->session->userdata('Usu_Id'));
        $query = $this->db->get();

        if ($query->num_rows()>0) {
            return true;
        }
        else
        {
            return false;
        }
    }
}

?>
