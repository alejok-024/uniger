<?php
class Reservas_Model extends CI_Model {

    public function crear_reserva($datos){
        

        $this->db->insert('Reservas', $datos);

        return true;
    }

    public function Find_reserva($id){
        
        $this->db->select('Res_Cantidad_Personas, Res_Estado, Res_Fecha_Inicio, Res_Fecha_Fin, Res_Tipo_Evento, Token');
        $this->db->from('Reservas');

        $this->db->like('Token', $id);

        $sql=$this->db->get(); 
        return $sql->result();

        return true;
    }

    public function Editar_Reserva($datos, $id){
        
        $this->db->set($datos);
        $this->db->where('Token', $id);
        $this->db->update('Reservas');

        return true;
    }

    public function Delete_Reserva($id){
        $this->db->delete('Reservas', array('Token' => $id));
        return true;
    }

    public function FindAll_reserva(){
        
        $sql=$this->db->query(" SELECT  Res_Tipo_Evento AS title, 
                                        SUBSTRING(Res_Fecha_Inicio FROM 1 FOR 10) AS start, 
                                        SUBSTRING(Res_Fecha_Fin FROM 1 FOR 10) AS end, 
                                        Token AS id 
                                FROM Reservas 
                                WHERE MONTH(Res_Fecha_Inicio) = MONTH(NOW())
                                ");

        

        return $sql->result();
    }
}
?>