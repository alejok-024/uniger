<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_Model extends CI_Model {
    
    /**
     * Cuarda un usuario en la base de datos.
     * @param $datos[]
     * @return N/A
     */
    public function registarUsuario($datos)
    {
        //ejecuto función
        $this->db->set('Usu_Fecha_Registro', 'NOW()', FALSE);
        $this->db->insert('Usuarios', $datos);
    }

    /**
     * Cuarda un usuario en la base de datos.
     * @param $datos[]
     * @return N/A
     */
    public function registarLogUsuario($datos)
    {
        //ejecuto función
        $this->db->set('Reg_Fecha_Registro', 'NOW()', FALSE);
        $this->db->insert('Registro_Detalle', $datos);
    }
    
    public function obtenerId($datos){
        $email = $datos["email"];
        //Busco el id por el email
        $this->db->select('id');    
    
        $this->db->from('usuarios');
    
        $this->db->where('email', $email);
        
        $query = $this->db->get();
        
        echo var_dump($query);
        die();
            /*if ( $query->num_rows() > 0 )
            {
                $row = $query->row_array();
                return $row;
            }*/
    }

    public function buscarUsuario($datos){

        $email = $datos["email"];
        $nombres = $datos["nombre"];
        $codigo = $datos["codigo"];
        //Busco el id por el email
        $sentence = "id_usuario, nombre, apellido, email, estado, (SELECT CONCAT(nombre, ' ' ,apellido) FROM ls_usuarios WHERE `codigo` = '$codigo') AS partnerName";
        $this->db->select($sentence);
        $this->db->from('ls_usuarios');
        $this->db->like('email', $email, 'both'); 
        $this->db->or_like('nombre', $nombres, 'both'); 
        $this->db->or_like('apellido', $nombres, 'both'); 
        $this->db->or_where('codigo =', $codigo);

        $query = $this->db->get();
        
        echo var_dump($query);
        echo $this->db->last_query();
        die();
            /*if ( $query->num_rows() > 0 )
            {
                $row = $query->row_array();
                return $row;
            }*/
    }

    /**
     * Traer todos los usuarios excepto el propio.
     * @param int $idUsuario 
     * @return array-booleano
     */
    public function getUsuarios($idUsuario) {

        $this->db->where('id_usuario !=', $idUsuario);
        $query = $this->db->get('ls_usuarios');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return FALSE;

    }

    /**
     * Description
     * @param type $nombreCompleto 
     * @return type
     */
    public function getIdUsuario($nombreCompleto) {

        $this->db->select('id_usuario');
        $this->db->where("CONCAT(nombre,' ',apellido) =", $nombreCompleto);
        $query = $this->db->get('ls_usuarios');
        if($query->num_rows() > 0 ) {
            return $query->row();
        }      
        return FALSE;

    }
   
    /**
     * Obtiene los usuarios nuevos y pendientes por aceptar o rechazar
     * @param type $nombreCompleto 
     * @return type
     */
    public function obtenerUsuariosNuevos()
    {
        $this->db->select("*");
       
        $this->db->where('Usu_Estado', '0');
        $query = $this->db->get('Usuarios');
        if($query->num_rows() > 0 ) {
            return $query->result();
        }      
        return FALSE;

    }

    /**
     * Guarda en la tabla Usuarios cuando un usuario es aprobado
     * @param type $nombreCompleto 
     * @return type
     */
    public function aprobarUsuario($incDatos)
    {
        $this->db->set('Usu_Estado', 'Aprobado');
        $this->db->set('Usu_Comentario', $incDatos['Usu_Comentarios']);
        $this->db->set('Usu_Rol', $incDatos['Usu_Rol']);
        $this->db->where('Usu_Id', $incDatos['Usu_Id']);
        $this->db->update('Usuarios');

    }

    /**
     * Guarda en la tabla Registro_Detalle cuando un usuario es aprobado o rechazado
     * @param type $nombreCompleto 
     * @return type
     */
    public function logRegistroUsuario($id, $estado,  $comentario="")
    {
        // var_dump($id);
        // die();
        if ($comentario === "") {
            $this->db->set('Reg_Estado', $estado);
            $this->db->where('Reg_Id', $id);
            $this->db->update('Registro_Detalle');
        }else{
            $this->db->set('Reg_Estado', $estado);
            $this->db->set('Reg_Comentario', $comentario);
            $this->db->where('Reg_Id', $id);
            $this->db->update('Registro_Detalle');
        }

    }

    /**
     * Guarda en la tabla InmuebleXUsuarios el inmueble y el usuario al que está asociado
     * @param type $nombreCompleto 
     * @return type
     */
    public function asociaUsuarioeInmueble($Usu_Id, $Inm_Id, $InmXUsu_Rol)
    {
        $this->db->set('Usu_Id', $Usu_Id);
        $this->db->set('Inm_Id', $Inm_Id);
        $this->db->set('InmXUsu_Rol', $InmXUsu_Rol);
        $this->db->insert('InmueblesXUsuarios');

    }

    /**
     * Obtiene los usuarios nuevos y pendientes por aceptar o rechazar
     * @param type $nombreCompleto 
     * @return type
     */
    public function rechazarUsuario($id)
    {
        $this->db->where('Usu_Id', $id);
        $this->db->delete('Usuarios');
    }    

    /**
     * Obtiene los usuarios nuevos y pendientes por aceptar o rechazar
     * @param type $nombreCompleto 
     * @return type
     */
    public function obtenerHistorialUsuarios()
    {
        $this->db->select('Reg_Id, Reg_Nombres, Reg_Apellidos, Reg_Telefono, Reg_Email, Reg_Estado, Reg_Apto,Reg_Comentario, Reg_Fecha_Registro');
        $this->db->from('Registro_Detalle');
        $this->db->group_by('Reg_Email');
        $this->db->order_by('Reg_Fecha_Registro DESC');
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result();
        }
        else
        {
            return false;
        }
    }    

    /**
     * Obtiene los intentos de registro de un usuario determinado
     * @param type $email 
     * @return null
     */
    public function detalleRegistroPorUsuario($email)
    {
        $this->db->select('*');
        $this->db->from('Registro_Detalle');
        $this->db->where('Reg_Email',$email);
        $this->db->order_by('Reg_Fecha_Registro DESC');
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result();
        }
        else
        {
            return false;
        }
    }  
    
    /**
     * Obtiene todos los usuarios registrados y aprobados en el sistema y los envia a la vista
     * @param type $nombreCompleto 
     * @return type
     */
    public function listarUsuarios()
    {
        $this->db->select('*');
        $this->db->from('Usuarios');
        $this->db->where('Usu_Estado', '1');
        
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            
            return $result->result();
        }
        else
        {
            return false;
        }
    } 
    public function detalleUsuarios($Id)
    {
        $this->db->select('*');
        $this->db->from('Usuarios');
        $this->db->where('Usu_Id', $Id);
        
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            
            return $result->result();
        }
        else
        {
            return false;
        }
    }

    /**
     * Obtiene todos los usuarios registrados y aprobados en el sistema y los envia a la vista
     * @param type $nombreCompleto 
     * @return type
     */
    public function detalleUsuario($id)
    {
        $this->db->select('*');
        $this->db->from('Usuarios');
        $this->db->where('Usu_Id',  $id);
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result()[0];
        }
        else
        {
            return false;
        }
    } 

    /**
     * Actualiza la información de usuario 
     * @param type $nombreCompleto 
     * @return type
     */
    public function actualizarUsuario($datos, $id)
    {
        try {
            $this->db->where('Usu_Id', $id);
            $this->db->update('Usuarios', $datos);
            
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        } finally {
            return true;
        }

    } 

    /**
     * Retorna los campos de información de un usuario
     * @param type $email, $contrasena
     * @return type array 
     */
    public function validarSiPerfilEstaCompleto($email, $contrasena)
    {
        try {
            $this->db->select('Usu_Tipo_Documento, Usu_Numero_Documento, Usu_Fecha_Nacimiento ');
            $this->db->from('Usuarios');
            $this->db->where('Usu_Email', $email);
            $this->db->where('Usu_Contrasena', sha1(md5($contrasena)));
            $query = $this->db->get();
            return $query->result()[0];
            
        } catch (Exception $e) {

            echo 'Excepción capturada: ',  $e->getMessage(), "\n";

        }

    } 

    /**
     * Retorna los campos de información de un usuario
     * @param type $email, $contrasena
     * @return type array 
     */
    public function completarInfo($datos)
    {
        try 
        {
            $this->db->where('Usu_Email', $this->session->userdata('Usu_Email'));
            $this->db->update('Usuarios', $datos);

        } catch (Exception $e) {

            echo 'Excepción capturada: ',  $e->getMessage(), "\n";

        }

    } 


    /**
     * Retorna los inmuebles y roles asociados a un usuario
     * @param type $email, $contrasena
     * @return type array 
     */
    public function Usuarios($datos)
    {
        try 
        {
            $this->db->where('Usu_Email', $this->session->userdata('Usu_Email'));
            $this->db->update('Usuarios', $datos);

        } catch (Exception $e) {

            echo 'Excepción capturada: ',  $e->getMessage(), "\n";

        }

    } 
    
    /**
     * Actualiza el rol de un usuario en la tabla InmueblesXUsuarios
     * @param type $email, $contrasena
     * @return type array 
     */
    public function actualizarInmueblesxUsuario($Rol, $Usu_Id, $Inm_Id)
    {
        try 
        {
            $this->db->where('Usu_Id', $Usu_Id);
            $this->db->where('Inm_Id', $Inm_Id);
            $this->db->set('InmXUsu_Rol', $Rol);
            $this->db->update('InmueblesXUsuarios');

        } catch (Exception $e) {

            echo 'Excepción capturada: ',  $e->getMessage(), "\n";

        }

    } 


    /********************************************/
    /*               PERFIL                     */
    /********************************************/

    /**
     * Método que obtiene toda la información personal de un usuario
     * @param type $id
     * @return type array 
     */
    public function obtener_Info_Usuario($id)
    {
        $this->db->select("*");
        $this->db->where("Usu_Id", $id);
        $query = $this->db->get("Usuarios");

        return $query->result()[0];        
    }

    /**
     * Método que obtiene toda la información personal de un usuario
     * @param type $id
     * @return type array 
     */
    public function password_check($password)
    {
        $usu_id = $this->session->userdata('Usu_Id');
        $this->db->select("Usu_Contrasena");
        $this->db->where("Usu_Id", $usu_id);
        $this->db->where("Usu_Contrasena", $password);
        $query = $this->db->get("Usuarios");

        if ($query->num_rows()>0) {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Establece una nueva contraseña en la base de datos
     * @param type $id
     * @return type array 
     */
    public function actualizarContrasena($password)
    {
        try
        {
            $usu_id = $this->session->userdata('Usu_Id');
            $this->db->set("Usu_Contrasena", $password);
            $this->db->where("Usu_Id", $usu_id);
            $query = $this->db->update("Usuarios");
    
            return true;
        }
        catch(exeption $e)
        {
            return $e;
        }
    }
    
    /**
     * Establece una nueva contraseña en la base de datos
     * @param type $id
     * @return type array 
     */
    public function actualizarInfoUsuario($data)
    {
        try
        {
            $this->db->update('Usuarios', $data);
            $this->db->where('Usu_Id', $this->session->userdata('Usu_Id'));
            
            return true;
        }
        catch(exeption $e)
        {
            return $e;
        }
    }

    /**
     * Consulta la cantidad de usuarios por rol en la tabla InmueblesXUsuarios
     * @param type $id
     * @return type array 
     */
    public function rolesxUsuarios()
    {
        try
        {
            $this->db->select('COUNT(`InmXUsu_Rol`) as conteoCategorias, `InmXUsu_Rol`');
            $this->db->group_by('InmXUsu_Rol');
            $query = $this->db->get('InmueblesXUsuarios');
            
            return $query->result();
        }
        catch(exeption $e)
        {
            return $e;
        }
    }

    /**
     * consulta la cantidad de registros de la tabla InmueblesXUsuarios
     * @param type $id
     * @return type array 
     */
    public function cantidadRolesxUsuarios()
    {
        try
        {
            $this->db->select('COUNT(`InmXUsu_Rol`) as conteoTotalCategorias');
            $query = $this->db->get('InmueblesXUsuarios');
            
            return $query->result()[0];
        }
        catch(exeption $e)
        {
            return $e;
        }
    }

    /**
     * consulta la cantidad de registros de la tabla InmueblesXUsuarios
     * @param type $id
     * @return type array 
     */
    public function reporteAprobaciones()
    {
        try
        {
            $this->db->select('COUNT(`Reg_Estado`) as conteoEstados, `Reg_Estado`');
            $this->db->group_by('Reg_Estado');
            $query = $this->db->get('Registro_Detalle');
            
            return $query->result();
        }
        catch(exeption $e)
        {
            return $e;
        }
    }

        /**
     * consulta la cantidad de registros de la tabla InmueblesXUsuarios
     * @param type $id
     * @return type array 
     */
    public function totalUsuariosRegistrados()
    {
        try
        {
            $this->db->select('COUNT(`Reg_Estado`) as conteoTotalEstados');
            $query = $this->db->get('Registro_Detalle');
            
            return $query->result()[0];
        }
        catch(exeption $e)
        {
            return $e;
        }
    }

    public function aprobarUsuarios($id)
    {
        
        $this->db->set('Usu_Estado',1);
        $this->db->Where('Usu_Id', $id);
        $this->db->Update('Usuarios');
        $query = $this->db->affected_rows();
       
    
        return $query;
    }
    public function eliminarUsuarios($id)
    {
    $this->db->delete('Usuarios', array(
        'Usu_Id' => $id
    ));
    return true;
    }
    public function detalleUsuariosNuevos($id)
    {
        $this->db->select("*");
        $this->db->where('Usu_Id', $id);
        $this->db->where('Usu_Estado', '0');
        $query = $this->db->get('Usuarios');
        if($query->num_rows() > 0 ) {
            return $query->result();
        }      
        return FALSE;

    }
    
}   