<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="pages-inicio-inmuebles.html">Inmuebles</a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="pages-consultar-inmueble.html">Consultar inmueble</a>
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Información inmueble</h4>
                </div>
            </div>
        </div>
        <div class="card m-b-30">
            <div class="card-body">
                <h3 class="m-t-0">
                    <img src="<?php echo base_url('application/views/'); ?>assets/images/logo-dark.png" alt="logo" height="32" />
                </h3>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="p-20">
                            <?php echo form_open('Inmuebles/Pdf_inmueble');?>
                            <div class="form-group">
                            <table class="table table-striped">
                <!-- Encabezado -->
                <thead>
                    <tr>
                        <div class="card-header">
                            <form>
                                <h6>
                                    <i class="dripicons-view-list"></i>
                                    &nbsp; Información inmueble

                                </h6>
                            </form>
                        </div>

                        <th>Tipo de inmueble</th>
                        <th>Dirección</th>
                        <th>Usuarios</th>
                        <th>Rol</th>
                        <th>Numero de apartamento</th>
                        <th>Numero de matricula</th>
                        <th>Area construida</th>
                        <th>Area privada</th>
                        <th>Coeficiente de propiedad</th>
                    </tr>
                </thead>
                <!-- Fin Encabezado -->
                <!-- Cuerpo -->
                <tbody>
                    <tr>
                    
                        <td>
                        <?php echo $inmuebles->Inm_Tipo; ?>
                        <td>
                        <?php echo $inmuebles->Inm_Direccion; ?>
                        <td>
                        <?php echo $inmuebles->Usu_Nombres; ?>
                        <?php echo $inmuebles->Usu_Apellidos; ?>
                        <td>
                        <?php echo $inmuebles->InmXUsu_Rol; ?>
                        <td>
                        <?php echo $inmuebles->Inm_Id; ?></td>
                            <td>
                            <?php echo $inmuebles->Inm_Numero_Matricula; ?> </td>
                            <td>
                            <?php echo $inmuebles->Inm_Area_Construida?> </td>
                            <td>
                            <?php echo $inmuebles->Inm_Area_Privada; ?> </td>
                            <td>
                            <?php echo $inmuebles->Inm_Coeficiente_Propiedad; ?></td>
                       
                    </tr>
               
                </tbody>
                <!-- Fin Cuerpo -->
            </table>
                            </div>
                            <input type="hidden" name="inmueble_id" value="<?php echo $id; ?>">
                            <?php echo form_close();?>
                            <div class="d-print-none">
                            <div class="pull-center">
                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>

        <br>
        <?php $this->load->view('footer'); ?>