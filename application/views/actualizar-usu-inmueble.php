<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="pages-inicio-inmuebles.html">Inmuebles</a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="pages-consultar-inmueble.html">Consultar inmueble</a>
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Cambiar Rol</h4>
                </div>
            </div>
        </div>
        <div class="card m-b-30">
            <div class="card-body">
                <h3 class="m-t-0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-20">
                                <?php echo form_open('inmuebles/Actulizar_Usu');?>
                                <div class="form-group">
                                    <label>Rol </label>
                                    <select class="form-control" name="InmXUsu_Rol">
                                        <option value="Arrendatario">Arrendatario</option>
                                        <option value="Inquilino">Inquilino</option>
                                        <option value="Comodatario">Comodatario</option>
                                        <option value="Albacea">Albacea</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Actualizar</button>
                            </div>
                            <input type="hidden" name="inmueble_id" value="<?php echo $id; ?>">
                            <?php echo form_close();?>
                        </div>
                    </div>
            </div>
        </div>

        <br>
        <?php $this->load->view('footer'); ?>