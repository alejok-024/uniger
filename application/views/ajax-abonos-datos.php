<?php 
$total = 0; 
$total_multas = 0;
$total_cuotas = 0;
$total_presupuesto = 0;
    
?>
<?php if(!empty($multas)){ ?>
    <?php foreach($multas as $multa){ ?>
        <?php $total = $total + $multa->Mul_Valor ?>
        <?php $total_multas = $total_multas + $multa->Mul_Valor ?>
    <?php } ?>
        <div class="form-group">
            <label for="input-multa">Valor multa (COP)</label>
            <input class="form-control" disabled value="<?php echo number_format($total_multas);?>">
        </div>
<?php } ?>
<?php if(!empty($cuotas_extraordinarias)){ ?>
    <?php foreach($cuotas_extraordinarias as $cuota){ ?>
        <?php $total = $total + $cuota->Det_Cuo_Ext_Valor; ?>
        <?php $total_cuotas = $total_cuotas + $cuota->Det_Cuo_Ext_Valor; ?>
    <?php } ?>
        <div class="form-group">
            <label for="input-multa">Valor cuota extraordinaria (COP)</label>
            <input class="form-control" disabled value="<?php echo number_format($total_cuotas);?>">
        </div>
<?php } ?>
<?php if(!empty($presupuestos)){ ?>
    <?php foreach($presupuestos as $presupuesto){ ?>
            <?php $total = $total + $presupuesto->Det_Pre_Valor; ?>
            <?php $total_presupuesto = $total_presupuesto + $presupuesto->Det_Pre_Valor; ?>
    <?php } ?>
        <div class="form-group">
            <label for="input-multa">Valor presupuesto (COP)</label>
            <input class="form-control" disabled value="<?php echo number_format($total_presupuesto);?>">
        </div>
<?php } ?>
<?php

if (empty($Abonos)) {
    $Abononado = 0;
}else
{
    $Abononado = 0;
    foreach($Abonos as $Abono)
    {
        $Abononado = $Abononado + $Abono->Abo_Valor;
    }
}

$total = $total - $Abononado;
if ($dias_vencido > 0) {
    $intereses_mes = $total * 0.251;
    $intereses_hoy = ($dias_vencido*$intereses_mes/30); 
    $total = ($total + $intereses_hoy);
}

?>
<?php if (isset($intereses_hoy)) {?>
    <div class="form-group">
    <label for="input-multa">Intereses de <?php echo $dias_vencido; ?> días(COP)</label>
    <input class="form-control" disabled value="<?php echo number_format($intereses_hoy);?>">
</div>
<?php } ?>
<div class="form-group">
    <label for="input-multa">Total a pagar (COP)</label>
    <input class="form-control" disabled value="<?php echo number_format($total);?>">
    <input type="hidden" name="input-total-pagar" id="input-total-pagar"  value="<?php echo round($total);?>">
    <input type="hidden" name="input-id-recibo-pago" id="input-id-recibo-pago"  value="<?php echo $id_recibo_pago;?>">
</div>
<?php if($total == 0 ){ ?>
<script>
    $.bsAlert.alert("Este recibo ya está pago");     
</script>
<?php } ?>