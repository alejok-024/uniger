/*
 Template Name: UNIGER
 Author: Alejandro
 File: Main js
 */

$(document).ready(function(){
    $('#imagenPrincipal').addClass('animated bounceInDown');
    $('#imagenPrincipal').hover(function(){
        $('#imagenPrincipal').removeClass('animated bounceInDown');
        $(this).addClass('animated tada');
    });
});