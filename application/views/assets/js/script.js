/*
 Content: Funciones javascript e invocación de librerías
 Author: Alejandro Velasco
 File: script.js
 */
	//Formato para campos que contengasn valores de dinero
	new Cleave('.input-val', {
		numeral: true,
		numeralDecimalMark: ',',
		delimiter: '.'
	});

	$(document).ready(function(){
		$("#General").DataTable();
	});