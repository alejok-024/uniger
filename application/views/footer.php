
                <!-- Footer -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                © 2018 UNIGER - Desarrollado con
                                <i class="mdi mdi-heart text-danger"></i> por J.A.L.Y.
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- Fin Footer -->


                <!-- jQuery  -->
                <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/popper.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/modernizr.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/waves.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.slimscroll.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.nicescroll.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.scrollTo.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/bootstrap-alert.js"></script>
                
                <!-- Required datatable js -->
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
                <!-- Buttons examples -->
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/jszip.min.js"></script>
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/pdfmake.min.js"></script>
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/vfs_fonts.js"></script>
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/buttons.html5.min.js"></script>
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/buttons.print.min.js"></script>
                <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/buttons.colVis.min.js"></script>
                <!-- Responsive examples -->
                <script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
                <script src="assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
                <!-- Datatable init js -->
                <script src="<?php echo base_url('application/views/');?>assets/pages/datatables.init.js"></script>
                <!-- Alertify js -->
                <script src="<?php echo base_url('application/views/'); ?>assets/plugins/alertify/js/alertify.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/pages/alertify-init.js"></script>

                <!-- Javascript -->
                <script src="<?php echo base_url('application/views/'); ?>assets/js/bootstrap-select.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/intro.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/app.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/ajax.js"></script>
                <!--<script src="<?php echo base_url('application/views/'); ?>assets/js/script.js"></script>-->


</body>

</html>