<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>UNIGER | UNIGER</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Themesdesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Iconos -->
    <link rel="shortcut icon" href="<?php echo base_url('application/views/'); ?>assets/images/favicon.ico">

    <!-- Css -->
    <link href="<?php echo base_url('application/views/'); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/bootstrap-alert.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/introjs.min.css" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

</head>


<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <!-- Barra de navegación -->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">

                <!-- Conetendor del logo-->
                <div class="logo">
                    <!-- Text Logo -->
                    <!--<a href="index.html" class="logo">-->
                    <!--Upcube-->
                    <!--</a>-->
                    <!-- Image Logo -->
                    <a href="index.html" class="logo">
                        <img src="<?php echo base_url('application/views/'); ?>assets/images/logo-sm.png" alt="" height="22" class="logo-small">
                        <img src="<?php echo base_url('application/views/'); ?>assets/images/logo.png" alt="" height="24" class="logo-large">
                    </a>

                </div>
                <!-- Fin Conetendor del logo -->

                <!-- Menu extras -->
                <div class="menu-extras topbar-custom">

                    <ul class="list-inline float-right mb-0">

                        <!-- Mensajes-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                aria-expanded="false">
                                <!--<i class="mdi mdi-email-outline noti-icon"></i>-->
                                <span class="badge badge-danger noti-icon-badge">3</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5>
                                        <span class="badge badge-danger float-right">745</span>Messages</h5>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon">
                                        <img src="<?php echo base_url('application/views/'); ?>assets/images/users/avatar-2.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                    <p class="notify-details">
                                        <b>Charles M. Jones</b>
                                        <small class="text-muted">Dummy text of the printing and typesetting industry.</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon">
                                        <img src="<?php echo base_url('application/views/'); ?>assets/images/users/avatar-3.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                    <p class="notify-details">
                                        <b>Thomas J. Mimms</b>
                                        <small class="text-muted">You have 87 unread messages</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon">
                                        <img src="<?php echo base_url('application/views/'); ?>assets/images/users/avatar-4.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                    <p class="notify-details">
                                        <b>Luis M. Konrad</b>
                                        <small class="text-muted">It is a long established fact that a reader will</small>
                                    </p>
                                </a>

                                <!-- Todos-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    View All
                                </a>

                            </div>
                        </li>
                        <!-- notificaciones-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                aria-expanded="false">
                                <!--<i class="mdi mdi-bell-outline noti-icon"></i>-->
                                <span class="badge badge-danger noti-icon-badge">3</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5>Notification (3)</h5>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-success">
                                        <i class="mdi mdi-cart-outline"></i>
                                    </div>
                                    <p class="notify-details">
                                        <b>Your order is placed</b>
                                        <small class="text-muted">Dummy text of the printing and typesetting industry.</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-warning">
                                        <i class="mdi mdi-message"></i>
                                    </div>
                                    <p class="notify-details">
                                        <b>New Message received</b>
                                        <small class="text-muted">You have 87 unread messages</small>
                                    </p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info">
                                        <i class="mdi mdi-martini"></i>
                                    </div>
                                    <p class="notify-details">
                                        <b>Your item is shipped</b>
                                        <small class="text-muted">It is a long established fact that a reader will</small>
                                    </p>
                                </a>

                                <!-- Todo-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    View All
                                </a>

                            </div>
                        </li>
                        <!-- Usurio-->
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                aria-expanded="false">
                                <?php  
                                   $url = $_SERVER['DOCUMENT_ROOT'] ."/uniger/application/views/imgPerfil/perfil".$this->session->userdata('Usu_Id').".jpg";
                                   if (file_exists($url)) {
                                         $url =  base_url("application/views/imgPerfil/perfil".$this->session->userdata('Usu_Id')).".jpg";
                                   }
                                   else
                                   {
                                         $url = base_url("application/views/assets/images/user-placeholder.png");
                                   }
                                ?>
                                <img src="<?php echo $url; ?>" alt="foto perfil" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <a class="dropdown-item" href="<?php echo base_url('/perfil');?>">
                                    <i class="dripicons-user text-muted"></i> Perfil
                                </a>
                                <a class="dropdown-item" href="<?php echo base_url('usuarios/guia');?>">
                                    <i class="mdi mdi-map  text-muted text-wrap"></i> Mapa
                                </a>
                                <a class="dropdown-item" href="#" id="help">
                                    <i class="mdi mdi-help text-muted text-wrap"></i> Ayuda
                                </a>
                                <a class="dropdown-item" href="<?php echo base_url('usuarios/acercaDe');?>">
                                    <i class="mdi mdi-apple-keyboard-command  text-muted text-wrap"></i> Acerca de
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo base_url();?>Security/salir">
                                    <i class="dripicons-exit text-muted"></i> Salir
                                </a>
                            </div>
                        </li>
                        <!-- Fin usuario -->
                        <li class="menu-item list-inline-item">
                            <!-- menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- fin menu toggle-->
                        </li>

                    </ul>
                </div>
                <!-- Fin menu-extras -->

                <div class="clearfix"></div>

            </div>
            <!-- fin container -->
        </div>
        <!-- fin topbar-main -->

        <!-- MENU -->
        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <?php $i = 1; ?>
                    <ul class="navigation-menu">
                        <!-- item-->
                        <li class="has-submenu">
                            <a href="<? echo base_url('/inicio');?> " data-step="<?php echo $i++ ?>" data-intro="Este botón siempre le llevará al inicio de la plataforma" data-position='bottom'>
                                <i class="ti-flag"></i>Inicio</a>
                        </li>
                        <?php if ($this->session->userdata('Usu_Rol') == 0) { ?>
                        <!-- item-->
                        <li class="has-submenu">
                            <a href="<? echo base_url('/inmuebles');?>" data-step="<?php echo $i++ ?>" data-intro="El módulo de inmuebles se encarga de gestionar toda la información relacionada con los apartamentos, cuarto útil y parqueadero del conjunto residencial Torres de la Giralda, el rol administrador es el único que tiene acceso a todas las funciones de este módulo." data-position='bottom'>
                                <i class="ti-home"></i>Inmuebles</a>
                        </li>
                        <?php } ?>
                        <!-- item-->
                        <li class="has-submenu">
                            <a href="<? echo base_url('/recibos');?>" data-step="<?php echo $i++ ?>" data-intro="El módulo recibos es el encargado de gestionar los recibos de pago de administración de cada uno de los inmuebles y los abonos que se pueden hacer a estos pagos, el rol administrador es el único que tiene acceso a todas las funciones de este módulo." data-position='bottom'>
                                <i class="ti-wallet"></i>Recibos</a>
                        </li>
                        <!-- item-->
                        <li class="has-submenu">
                             <a href="<? echo base_url('/zonacomun');?>" data-step="<?php echo $i++ ?>" data-intro="El módulo de zonas comunes se encarga de gestionar toda la información relacionada con las reservas, los mantenimientos, las zonas comunes y los proveedores del conjunto residencial Torres de la Giralda, el rol administrador tiene acceso a todas las funciones de este módulo y el rol usuario solo puede acceder a ciertas funciones." data-position='bottom'>
                                <i class="dripicons-contract"></i>Zonas Comunes</a>
                        </li>
                        <!-- item-->
                        <li class="has-submenu">
                        <a href="<? echo base_url('/Publicaciones');?>" data-step="<?php echo $i++ ?>" data-intro="El módulo de comunicaciones permite a los usuarios realizar publicaciones o denuncias, también permite al administrador generar notificaciones (Mensajes), lo cual permite la comunicación directa entre el administrador y residentes del conjunto residencial." data-position='bottom'>
                                <i class="dripicons-conversation"></i>Comunicaciones</a>
                        </li>
                        <?php if ($this->session->userdata('Usu_Rol') == 0) { ?>
                            <!-- item-->
                            <li class="has-submenu">
                                <a href="<? echo base_url('/usuarios');?>" data-step="<?php echo $i++ ?>" data-intro="El módulo usuarios es el encargado de gestionar los roles y los permisos que se le asigna a cada uno de los usuarios registrados dentro de la plataforma, además de generar reportes en cuanto a los registros y los diferentes tipos de perfiles que maneja el software, el rol administrador es el único que tiene acceso a todas las funciones de este módulo." data-position='bottom'>
                                    <i class="dripicons-user-group"></i>Usuarios</a>
                            </li>
                        <?php }  ?>
                    </ul>
                    <!-- Fin navigation menu -->
                </div>
                <!-- Fin #navigation -->
            </div>
            <!-- Fin container -->
        </div>
        <!-- Fin navbar-custom -->
    </header>
    <!-- Fin MENU -->
