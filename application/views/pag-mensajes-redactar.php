<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="pages-inicio-comunicaciones.html">Comunicaciones</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <a href="pages-mensajes.html">Mensajes</a>
                                </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Redactar mensaje</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin titulo pagina y miga de pan -->
        <center>
            <div class="col-8">
                <div class="card m-b-40">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Para:</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" id="example-text-input">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">Asunto</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="search" id="example-search-input">
                            </div>
                        </div>
                        <hr>
                        <label>Descripción</label>
                        <textarea class="form-control" id="Descripcion" rows="3" name="mensaje" value=""></textarea>                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" onclick="form.submit();this.disabled=true">Enviar</button>
                    </div>
                </div>
                    </div>
                  
            </div>
        </center>
        <!-- end col -->

        <?php $this->load->view('footer'); ?>