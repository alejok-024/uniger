<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>UNIGER | UNIGER</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Themesdesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Iconos -->
    <link rel="shortcut icon" href="<?php echo base_url('application/views/'); ?>assets/images/favicon.ico">

    <!-- Css -->
    <link href="<?php echo base_url('application/views/'); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/style.css" rel="stylesheet" type="text/css" />

</head>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-block">

                    <div class="ex-page-content text-center">
                        <h1 class="">404!</h1>
                        <h3 class="">Lo sentimos, página no encontrada</h3><br>

                        <a class="btn btn-info mb-5 waves-effect waves-light" href="<?php echo base_url('/inicio'); ?>">Volver al inicio</a>
                    </div>

                </div>
            </div>
        </div>

                <!-- jQuery  -->
                <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/popper.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/modernizr.min.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/waves.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.slimscroll.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.nicescroll.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.scrollTo.min.js"></script>
                <!-- Alertify js -->
                <script src="<?php echo base_url('application/views/'); ?>assets/plugins/alertify/js/alertify.js"></script>
                <script src="<?php echo base_url('application/views/'); ?>assets/pages/alertify-init.js"></script>

                <!-- Javascript -->
                <script src="<?php echo base_url('application/views/'); ?>assets/js/app.js"></script>

</body>

</html>