<?php $this->load->view('header'); ?>


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                                    <li class="breadcrumb-item"><a href="#">Facturacion</a></li>
                                    <li class="breadcrumb-item"><a href="#">Recibos de pago</a></li>
                                    <li class="breadcrumb-item active">Recibo</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Recibo de pago</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
            <?php echo form_open('Recibos/Imprimir_Recibo'); ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-12">
                                        <div class="invoice-title">
                                            <h4 class="pull-right font-16"><strong>Recibo de pago # 001</strong></h4>
                                            <h3 class="m-t-0">
                                                <img src="assets/images/logo-dark.png" alt="logo" height="32"/>
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group row">
                                            <div class="col-md-6">
                                              <label>inquilino</label>
                                            </div>
                                            <div class="col-md-6">
                                              <label>Direccion</label>
                                            </div>
                                            
                                            </div>
                                            

                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <select class="form-control" name="inquilino">
                                                        <option value="">Seleccione</option>
                                                        <?php foreach($recibos as $recibo): ?>
                                                    <option value="<?php echo $recibo->Usu_Id;?>"><?php echo $recibo->Usu_Nombres." ".$recibo->Usu_Apellidos;?></option>
                                                    <?php endforeach; ?> 
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <input readonly class="form-control" type="text" value="Torres de la giralda" name="Direcccion">
                                                </div>
                                                
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                <div class="col-md-6">
                                                  <label>N° de Apartamento</label>
                                                  
                                                </div>
                                                <div class="col-md-6">
                                                  <label>Fecha</label>
                                                </div>
                                                
                                                </div>
                                                    <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <input class="form-control" value="<?php echo $informacion->Inm_Id ?>" type="number" placeholder="" name="Inm_Id">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input name="Fecha"  class="form-control" value="<?php echo date("Y-m-d")
                                                        ?>">
                                                    </div>
                                                    
                                                    </div>
                                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="panel panel-default">
                                            <div class="p-2">
                                                <h3 class="panel-title font-20"><strong>Resumen de orden</strong></h3>
                                            </div>
                                            
                                                    <div class="card-body">
                                                        <div class="form-group row">
                                                        <div class="col-md-4    ">
                                                          <label>Detalles</label>
                                                        </div>
                                                        <div class="col-md-4">
                                                          <label>Precio</label>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Cantidad</label>
                                                          </div>
                                                        
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-4    ">
                                                              <label>Administracion</label>
                                                            </div>
                                                            
                                                            <div class="col-md-4">
                                                             <label><input type="number" class="form-control" name="Administracion"></label>
                                                             <?php echo form_error('Administracion'); ?>
                                                              </div>
                                                            
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-md-4">
                                                                  <label>Salon social</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                <label><input type="number" class="form-control"  name="Salon"></label>
                                                                <?php echo form_error('Salon'); ?>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label><input type="number" class="form-control" name="salon_Cantidad"></label>
                                                                    <?php echo form_error('salon_Cantidad'); ?>
                                                                  </div>
                                                                
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-4    ">
                                                                      <label>Multas</label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                    <label><input type="number" class="form-control" name="Multas"></label>
                                                                    <?php echo form_error('Multas'); ?>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <label><input type="number" class="form-control" name="multas_Cantidad"></label>
                                                                        <?php echo form_error('multas_Cantidad'); ?>
                                                                      </div>
                                                                    
                                                                    </div>
                                                                    <div class="offset-md-6">
                                                                        <button type="submit" class="btn btn-primary">Generar</button>
                                                                    </div>
                                                        </div>
                                              
                                        </div>

                                    </div>
                                </div> <!-- end row -->

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            <?php form_close(); ?>
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('footer'); ?>