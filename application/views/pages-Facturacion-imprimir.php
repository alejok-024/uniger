<?php $this->load->view('header'); ?>
<div class="wrapper">
<div class="container-fluid">
<!-- Page-Title -->
<div class="row">
   <div class="col-sm-12">
      <div class="page-title-box">
         <div class="btn-group pull-right">
            <ol class="breadcrumb hide-phone p-0 m-0">
               <li class="breadcrumb-item"><a href="<?php echo base_url("/inicio");?>">Inicio</a></li>
               <li class="breadcrumb-item"><a href="<?php echo base_url("/recibos");?>">Recibos de pago</a></li>
               <li class="breadcrumb-item active">Recibo</li>
            </ol>
         </div>
         <h4 class="page-title">Recibo de pago</h4>
      </div>
   </div>
</div>
<!-- end page title end breadcrumb -->
<?php echo form_open('Recibos/crear_Recibos'); ?>
<div class="row">
   <div class="col-12">
      <div class="card m-b-30">
         <div class="card-body">
            <div class="row">
               <div class="col-12">
                  <div class="invoice-title">
                     <h4 class="pull-right font-16"><strong>Recibo de pago # 001</strong></h4>
                     <h3 class="m-t-0">
                        <img src="<?php echo base_url('application/views/'); ?>assets/images/logo-dark.png" alt="logo" height="32"/>
                     </h3>
                  </div>
                  <div class="card-body">
                     <div class="form-group row">
                        <div class="col-md-6">
                           <label><b>N° de Apartamento*</label>
                           <?php echo form_error('N_Apartamento'); ?>
                        </div>
                        <div class="col-md-6">
                           <label>Fecha</label>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-md-6">
                           <select class="form-control" name="N_Apartamento" id="apartamento">
                              <option value="">Seleccione</option>
                              <?php foreach($recibos as $recibo): ?>
                                <option value="<?php echo $recibo->Inm_Id;?>"><?php echo $recibo->Inm_Id;?></option>
                                <?php endforeach; ?>
                           </select>
                        </div>
                        <div class="col-md-6">
                           <input name="Fecha" disabled class="form-control" value="<?php echo date("Y-m-d")
                              ?>">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="panel panel-default">
                           <div class="p-2">
                              <h3 class="panel-title font-20"><strong>Resumen de orden</strong></h3>
                           </div>
                           <div class="card-body">
                              <div class="form-group row">
                                 <div class="offset-lg-3 col-md-4">
                                    <label></label>
                                 </div>
                                 <div class="col-md-4">
                                    <label>Valor</label>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <div class="offset-lg-3 col-lg-4   ">
                                    <label>Administración*</label>
                                 </div>
                                 <div class=" col-md-4">
                                    <label><input type="number" class="form-control" name="Administracion"></label>
                                    <?php echo form_error('Administracion'); ?>
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <div class="offset-lg-3 col-md-4">
                                    <label>Cuota extraordinaria</label>
                                 </div>
                                 <div class="col-md-4">
                                    <label><input type="number" class="form-control"  name="extraordinaria"></label>
                                   
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <div class="offset-lg-3 col-md-4">
                                    <label>Multas</label>
                                 </div>
                                 <div class="col-md-4">
                                    <label><input type="number" class="form-control" name="Multas" id="Multas"></label>
                                    
                                 </div>
                              </div>
                              <div class="offset-md-6">
                                 <button type="submit" class="btn btn-primary">Generar</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- end row -->
               </div>
            </div>
         </div>
         <!-- end col -->
      </div>
      <!-- end row -->
      <?php form_close(); ?>
   </div>
   <!-- end container -->
</div>
<!-- end wrapper -->
<!-- Footer -->
<?php $this->load->view('footer'); ?>