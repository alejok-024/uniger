
<?php $this->load->view('header'); ?>


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="<?php echo base_url("/inicio");?>">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url("/recibos");?>">Recibos de pago</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url("/recibos/listar_recibo");?>">Listar recibos</a></li>
                        <li class="breadcrumb-item active">Abonos</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Recibo de pago</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-12">
                                <div class="invoice-title">
                                    <h4 class="pull-right font-16">
                                        <strong>Abonos</strong>
                                    </h4>
                                    <h3 class="m-t-0">
                                        <img src="<?php echo base_url('application/views/'); ?>assets/images/logo-dark.png" alt="logo" height="32" />
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <?php echo form_open('Recibos/crear_Abonos');?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="p-20">
                                            
                                                    <div class="form-group">
                                                        <label>Inquilino</label>
                                                        <input readonly type="text" placeholder="" value="<?php echo $recibos->Usu_Nombres." ".$recibos->Usu_Apellidos; ?>" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Apartamento</label>
                                                        <input readonly type="text" placeholder="" value="<?php echo $recibos->Inm_Id; ?>" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Valor Factura</label>
                                                        <input readonly type="text" placeholder="" name="valor_factura" value="<?php echo $recibos->Rec_Pag_Valor; ?>" class="form-control">
                                                    </div>

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="p-20">

                                                    <div class="form-group">
                                                        <label>Direccion</label>
                                                        <input readonly type="text" placeholder="" value="Torres de la giralda" class="form-control">                                  
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fecha del abono</label>
                                                        <input type="date" placeholder="" value="<?php echo date("Y-m-d") ?>" class="form-control" name="fecha_abono">
                                                        <?php echo form_error('fecha_abono'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Monto del abono</label>
                                                        <input type="number" step="any" placeholder="" class="form-control" min="1" name="valor_abono">
                                                        <input type="hidden" value="<?php echo $recibos->Rec_Pag_Id; ?>" name="id_factura">
                                                        <?php echo form_error('valor_abono'); ?>
                                                    </div>

                                            </div>
                                        </div> <!-- end col -->

                                    </div>
                                    <div class="row">
                                        <div class="offset-md-5">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg">Historial</button>
                                            <button type="submit" class="btn btn-primary">Generar</button>
                                        </div>                                    
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!-- modal -->
                                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Abonos</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Tabla -->
                                                    <table class="table table-striped">
                                                        <!-- Encabezado -->
                                                        <thead>
                                                            <tr>
                                                                <td>Valor Abonado</td>
                                                                <td>Valor Restante</td>
                                                                <td>Fecha Abono</td>
                                                                
                                                            </tr>
                                                        </thead><!-- Fin Encabezado -->
                                                        <!-- Cuerpo -->
                                                        <tbody>
                                                            <?php
                                                            $valorRestante = $recibos->Rec_Pag_Valor;
                                                            foreach ($historial as $historia) 
                                                            {
                                                                $valorRestante -=$historia->Abo_Pag_Valor;
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $historia->Abo_Pag_Valor; ?></td>
                                                                    <td><?php echo $valorRestante; ?></td>
                                                                    <td><?php echo $historia->Abo_Pag_Fecha; ?></td>

                                                                </tr>
                                                            <?php
                                                            } 
                                                            ?>
                                                        </tbody>
                                                    </table><!-- Fin Tabla -->
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- end modal -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- end wrapper -->
<!-- Footer -->
<?php $this->load->view('footer'); ?>
<?php 
    $success = $this->session->flashdata('success');
    if ($success) 
    {
    ?>
    <script>
        $(document).ready(function(){
            alertify.success("Proceso realizado con éxito");
        });
    </script>   
    <?php
    }
?>

