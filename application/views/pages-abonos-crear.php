<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('multas'); ?>">Inicio Abonos</a>
                                </li>
                                <li class="breadcrumb-item active">Abonos</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Abonos</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            
            <div class="row">

                    <!-- Col -->
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Abonos</h4>
                                <p class="text-muted m-b-30 font-14">Crear Abono.</p>
                                <?php $args= array('id' => 'crear_abono'); ?>
                                <?php echo form_open('Abonos/guardar', $args); ?>
                                    <div class="form-group">
                                        <label for="input-inmueble">Seleccione recibo*</label>
                                        <div>
                                            <select class="selectpicker col-lg-12" name="input-inmueble" id="input-recibo" data-live-search="true" title="Seleccione un recibo">
                                                <?php foreach ($recibos as $recibo) { ?>
                                                    <option data-tokens="<?php echo $recibo->Rec_Pag_Numero; ?>"><?php echo $recibo->Rec_Pag_Numero; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-none justify-content-center " id="contentLoader">
                                        <div class="loader"></div>
                                    </div>
                                    <div id="result">
                                        
                                    </div>


                                    <div class="form-group">
                                        <label for="input-multa">Valor a abonar (COP)</label>
                                        <input class="form-control input-val" id="input-abono" name="input-abono" required value="<?php echo set_value('input-abono'); ?>">
                                        <?php echo form_error('input-abono'); ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-success " id="submitAbono">Registrar pago</button>

                                        <button class="btn btn-success d-none" disabled id="loaderAbono"><span class="mr-2">Validando, por favor espere...</span><div class="loader"></div></button>
                                    </div>

                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>


        </div><!-- Fin container -->
    </div>
    <!-- Fin wrapper -->
    <script>
        var base_url = "<?php echo base_url(); ?>";
    </script>
    <?php $this->load->view('footer'); ?>