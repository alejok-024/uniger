<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- Contenedor -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Mantenimiento</a>
                            </li>
                            <li class="breadcrumb-item active">Consultar</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Calificar mantenimiento</h4>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <br>
                    <div class="card m-b-30">
                        <div class="card-header">
                             <div class="card-body">
                                <?php echo form_open('Mantenimiento/actualizar_mantenimiento');?>
                            <div class="form-group">
                                <label>Comentario final</label>
                                <textarea id="Comentarios" rows="3"  value="<?php echo $informacion->Man_Comentario_Final; ?>" class="form-control" name="Man_Comentario_Final" ></textarea>
                            </div>
                            <div class="form-group">
                                <label for="calificacion">
                                    <b>Calificación del Mantenimiento</label>
                                <select class="form-control" placeholder="Seleccione" id="tipomantenimiento" name="Man_Calificacion">
                                    <option>5</option>
                                    <option>4</option>
                                    <option>3</option>
                                    <option>2</option>
                                    <option>1</option>
                                </select>
                            </div>
                                    <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary">Actualizar</button>
                                    <input type="hidden" name="mantenimiento_id" value="<?php echo $id; ?>">
                                    </div>
                                 <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br>
            <?php $this->load->view('footer'); ?>
