<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- Contenedor -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Proveedores</a>
                            </li>
                            <li class="breadcrumb-item active">Consultar</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Informacion proveedor</h4>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <br>
                    <div class="card m-b-30">
                        <div class="card-header">
                             <div class="card-body">
                                <?php echo form_open('Proveedores/actualizar_proveedor');?>
                                <div class="form-group">
                                    <b>Tipo de Proveedor</label>
                                <select class="form-control" name="Pro_Tipo">
                                <option value="<?php echo $informacion->Pro_Tipo;?>"><?php echo $informacion->Pro_Tipo;?></option>
                                    <option>Mantenimiento</option>
                                    <option>Prevención</option>
                                    <option>Instalación</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nombre del Proveedor</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Pro_Nombre; ?>" class="form-control" name="Pro_Nombre">
                            </div>
                            <div class="form-group">
                                <label>Teléfono del Proveedor</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Pro_Telefono; ?>" class="form-control" name="Pro_Telefono">
                            </div>
                            <div class="form-group">
                                <label>Dirección del proveedor</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Pro_Direccion; ?>" class="form-control" name="Pro_Direccion">
                            </div>
                            <div class="form-group">
                                <label>E-mail del Proveedor</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Pro_Correo; ?>" class="form-control" name="Pro_Correo">
                            </div>
                            <div class="form-group">
                                <label>Contacto del Proveedor</label>
                                <input type="text" placeholder="" value="<?php echo $informacion->Pro_Contacto; ?>" class="form-control" name="Pro_Contacto">
                            </div>
                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea id="Descripción" rows="3"  value="<?php echo $informacion->Pro_Descripcion; ?>" class="form-control" name="Pro_Descripcion" ></textarea>
                            </div>
                                    <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary">Actualizar</button>
                                    <input type="hidden" name="proveedor_id" value="<?php echo $id; ?>">
                                    </div>
                                 <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br>
            <?php $this->load->view('footer'); ?>
