<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- Contenedor -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Zonas comunes</a>
                            </li>
                            <li class="breadcrumb-item active">Consultar</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Informacion zona comun</h4>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <br>
                    <div class="card m-b-30">
                        <div class="card-header">
                             <div class="card-body">
                                <?php echo form_open('ZonaComun/actualizar_zonacomun');?>
                                    <div class="form-group">
                                        <label>Nombre de Zona</label>
                                        <input type="text" placeholder="" value="<?php echo $informacion->Zon_Com_Nombre; ?>" class="form-control" name="Zon_Com_Nombre">
                                    </div>
                                    <div class="form-group">
                                        <label>Periodo de Mantenimiento</label>
                                        <input type="text" placeholder="" value="<?php echo $informacion->Zon_Periodo_Mantenimiento; ?>" class="form-control" name="Zon_Periodo_Mantenimiento" >
                                    </div>
                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <input type="text" placeholder="" value="<?php echo $informacion->Zon_Descripcion; ?>" class="form-control" name="Zon_Descripcion" >
                                    </div>
                                    <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary">Actualizar</button>
                                    </div>
                                    <input type="hidden" name="zonacomun_id" value="<?php echo $id; ?>">
                                 <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br>
            <?php $this->load->view('footer'); ?>
