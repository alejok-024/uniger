<?php $this->load->view('header'); ?>

       
        <!-- App css -->
        <link href="<?php echo base_url('application/views/'); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('application/views/'); ?>assets/css/FullCalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/js/FullCalendar/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/js/FullCalendar/moment.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">UNIGER</a></li>
                                    <li class="breadcrumb-item active">Calendario</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Calendario</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                    <div id='calendar' class="col-xl-10 col-lg-10 col-md-10" style="overflow: auto;"></div>
                                    <div class="col-xl-1 col-lg-1 col-md-1"></div>
                                </div>
                                <!-- end row -->

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->           

<?php $this->load->view('footer'); ?>


        <!-- Jquery-Ui -->
        
        <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/js/FullCalendar/fullcalendar.min.js"></script>
        
        <!-- App js -->
        <!-- <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/js/app.js"></script> -->

        <script type="text/javascript">
        
        $(document).ready(function() {

            var dt = new Date();

            // Display the month, day, and year. getMonth() returns a 0-based number.
            var month = dt.getMonth()+1;
            if(month<10){
                month='0'+month;
            } 
            var day = dt.getDate();
            var year = dt.getFullYear();

            var fechaactual = year + '-' + month + '-' + day;
            
            
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    // center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: function(start, end, timezone, callback){
                    $.ajax({
                        url: '<? echo base_url('Reservas/FindAll_Reserva');?>',
                        method: 'POST',
                        dataType: 'json',
                        success: function(response)
                        {   
                        callback(response);
                        },
                        async: false
                    });
                },
                defaultDate: fechaactual,
                navLinks: false, // can click day/week names to navigate views
                selectable: false,
                editable: false,
                eventLimit: true, // allow "more" link when too many events
          
            });
        });
            
        </script>