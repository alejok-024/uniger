<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- Contenedor -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                            <a href="<?php echo base_url('Inmuebles'); ?>">Inmuebles</a>                           
                            </li>
                        </ol>
                    </div>
                    <h2 class="page-title">Inmuebles</h2>
                </div>
            </div>
        </div>
        <!-- Fin titulo y mida de pan -->

        <div class="card m-b-30">
            <div class="card-header">
                <label>
                    <h5>Filtro de busqueda</h5>
                </label>
            </div>
            <div class="card-body">
                <form action="<? echo base_url('inmuebles/Buscar');?>" method="POST">

                    <!-- Filtro Busqueda -->
                    <form>
                        <div class="form-row">
                            <div class="col-md-2 mb-4">
                                <label>Número de inmueble</label>
                                <input type="number" name="Inm_Id" placeholder="Buscar.." class="form-control" value="<?php  echo  set_value ( 'Inm_Id' );  ?>">
                                <?php echo form_error('Inm_Id'); ?>
                            </div>
                            &nbsp;
                            <div class="col-md-2 mb-4">
                                <label>Nombre</label>
                                <input type="text" name="Usu_Nombres" placeholder="Buscar.." class="form-control" value="<?php  echo  set_value ( 'Usu_Nombres' );  ?>">
                                <?php echo form_error('Usu_Nombres'); ?>
                            </div>
                            &nbsp;
                            <div class="col-md-2 mb-4">
                                <label>Apellido</label>
                                <input type="text" name="Usu_Apellidos" placeholder="Buscar.." class="form-control" value="<?php  echo  set_value ( 'Usu_Apellidos' );  ?>">
                                <?php echo form_error('Usu_Apellidos'); ?>
                            </div>
                            &nbsp;
                            <div class="col-md-3 mb-4">
                                <label>Tipo de inmueble</label>
                                <input type="text" name="Inm_Tipo" placeholder="Buscar.." class="form-control" value="<?php  echo  set_value ( 'Inm_Tipo' );  ?>">
                                <?php echo form_error('Inm_Tipo'); ?>
                            </div>
                            &nbsp;
                            <div class="col-md-2 mb-4">
                                <label>Área construida</label>
                                <input type="number" name="Inm_Area_Construida" placeholder="Buscar.." class="form-control" value="<?php  echo  set_value ( 'Inm_Area_Construida' );  ?>">
                                <?php echo form_error('Inm_Area_Construida'); ?>
                            </div>
                        </div>
                        <button type="buscar" class="btn btn-primary">Buscar</button>
                    </form>
                </form>
            </div>
        </div>
        <!-- Resultados -->
        <!-- Contenido Principal -->
        <div class="row">
            <!-- Tabla -->
            <?php
            if(empty($inmuebles))
            {
                echo "<h3>No hubo resultados que concuerden con sus parámetros de búsqueda</h3>";
            }
            else { ?>
            <table class="table table-striped" id="datatable">
                <!-- Encabezado -->
                <thead>
                    <tr>
                        <div class="card-header">
                            <form>
                                <h6>
                                    <i class="dripicons-view-list"></i>
                                    &nbsp; Resultados
                                </h6>
                            </form>
                        </div>

                        <th>Número inmueble</th>
                        <th>Usuario</th>
                        <th>Tipo inmueble</th>
                        <th>Área construida</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <!-- Fin Encabezado -->
                <!-- Cuerpo -->
                <tbody>
                    <tr>
                        <?php
                         foreach($inmuebles as $inmueble){

                    ?>

                        <td>
                            <?php echo $inmueble->Inm_Id?> </td>
                        <td>
                            <?php echo $inmueble->Usu_Nombres." ".$inmueble->Usu_Apellidos?> </td>
                        <td>
                            <?php echo $inmueble->Inm_Tipo?> </td>
                        <td>
                            <?php echo $inmueble->Inm_Area_Construida?> </td>
                        <td>
                        <a href="#" class="btn btn-outline-danger waves-effect waves-light mr-2" onclick=" LanzarModal(<?php echo $inmueble->Inm_Id; ?>);">
                                <i class=" mdi mdi-delete"></i>
                            </a>
                            <a href="<?php echo base_url("inmuebles/editar_inmueble/".$inmueble->Inm_Id);?>" class="btn btn-outline-info waves-effect waves-light mr-2">
                                <i class="ion-edit"></i>
                            </a>
                        </td>
                    </tr>
                    <?php }?>
                    <?php }?>
                </tbody>
                <!-- Fin Cuerpo -->
            </table>
            <!-- Fin Tabla -->
        </div>

    </div>
    <!-- fin container -->
</div>
<!-- Modal DELETE -->
<div class="modal fade" id="ModalDeleteInmueble" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                        <div class="col-xl-10 col-lg-10 col-md-10">
                            <div class="form-group">
                                <p>¿Está seguro que desea eliminar el inmueble?</p>
                            </div>
                            
                            <input type="text" class="form-control" name="TokenDel" id="TokenDel" hidden>
                                
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="EliminarInmueble();">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
<script>
    var base_url = "<?php echo base_url();?>";
</script>
<?php $this->load->view('footer'); ?>

<script type="text/javascript">

function LanzarModal(ID){

    document.getElementById("TokenDel").value = ID;
    $('#ModalDeleteInmueble').modal('show');
}


function EliminarInmueble(){

    var TokenDel = document.getElementById("TokenDel");

    var Registro = {
        "Res_Token": TokenDel.value
    };

    $.ajax({
        url: '<? echo base_url('Inmuebles/borrar_inmueble/');?>',
        method: 'POST',
        data: Registro,
        success: function(response)
        {
            
            $('#ModalDeleteInmueble').modal('hide');
            window.location = '/uniger/inmuebles/Listar/delete/';

        },
        error: function (err) {
            alert("Ocurrió un error: " + JSON.stringify(err, null, 2));
        }
    });
    }

</script>
<!-- ALERTAS -->
<?php 
                if ($this->uri->segment(3)== "success")
                {?>
<script>
    $(document).ready(function () {
        alertify.success("Inmueble creado correctamente");
    });
</script>

<?php } else if ($this->uri->segment(3)== "delete") 
                {?>
<script>
    $(document).ready(function () {
        alertify.success("Inmueble borrado correctamente");
    });
</script>
<?php } else if ($this->uri->segment(3)== "update")
                {?>
<script>
    $(document).ready(function () {
        alertify.success("Inmueble actualizado correctamente");
    });
</script>
<?php } ?>
        <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('application/views/');?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Datatable init js -->
        <script src="<?php echo base_url('application/views/');?>assets/pages/datatables.init.js"></script>
<?php if ($this->session->flashdata('result')) {?>
 
<?php }; ?>
