<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- Contenedor -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Zonas comunes</a>
                            </li>
                            <li class="breadcrumb-item active">Consultar</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Consultar zona comun</h4>
                </div>
            </div>
        </div>

        <div class="card m-b-30">
            <div class="card-header">
                <label>
                    <h5>Filtro de busqueda</h5>
                </label>
            </div>
            <div class="card-body">
                <form action="<? echo base_url('zonacomun/Buscar');?>" method="POST">

                    <!-- Filtro Busqueda -->
                    <form>
                        <div class="row justify-content-md-center">
                            <div class="col-md-4 mb-4">
                                <label>Nombre de Zona</label>
                                <?php echo form_error('Zon_Com_Nombre'); ?>
                                <input type="text" name="Zon_Com_Nombre" placeholder="Buscar.." class="form-control" value= "<?php  echo  set_value ( 'Zon_Com_Nombre' );  ?>">
                            </div>
                            &nbsp;
                            <div class="col-md-4 mb-4">
                                <label>Periodo de Mantenimiento</label>
                                <?php echo form_error('Zon_Periodo_Mantenimiento'); ?>
                                <input type="text" name="Zon_Periodo_Mantenimiento" placeholder="Buscar.." class="form-control" value="<?php  echo  set_value ( 'Zon_Periodo_Mantenimiento' );  ?>">
                            </div>
                            &nbsp;
                           
                        </div>
                        <div class="d-flex justify-content-center">
                        <button type="buscar" class="btn btn-primary ">Buscar</button>
                        </div>
                    </form>
                </form>
            </div>
        </div>

        <div class="row">
            <!-- Tabla -->
            <?php
            if(empty($zonacomun))
            {
                echo "<h3>No hay resultados que concuerden con sus parámetros de búsqueda</h3>";
            }
            else { ?>
            <table class="table table-striped">
                <!-- Encabezado -->
                <thead>
                    <tr>
                        <div class="card-header">
                            <form>
                                <h6>
                                    <i class="dripicons-view-list"></i>
                                    &nbsp; Resultados

                                </h6>
                            </form>
                        </div>
                        <th>Nombre de Zona</th>
                        <th>Mantenimiento</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <!-- Fin Encabezado -->
                <!-- Cuerpo -->
                <tbody>
                    
                    <tr>
                    <?php foreach($zonacomun as $zoncom){?>
                        <td>
                            <?php echo $zoncom->Zon_Com_Nombre?> </td>
                        <td>
                            <?php echo "Cada ".$zoncom->Zon_Periodo_Mantenimiento." Dias"?> </td>
                        <td>
                            <?php echo $zoncom->Zon_Descripcion?> </td>
                        <td>
                            <a href="#" class="btn btn-outline-danger waves-effect waves-light mr-2" onclick=" LanzarModal(<?php echo $zoncom->Zon_Com_Id; ?>);">
                                <i class=" mdi mdi-delete"></i>
                            </a>
                            <a href="<?php echo base_url("ZonaComun/editar_zonacomun/".$zoncom->Zon_Com_Id);?>" class="btn btn-outline-info waves-effect waves-light mr-2">
                                <i class="ion-edit"></i>
                            </a>
                        </td>
                    </tr>
                    <?php }?>
                    <?php }?>
                </tbody>
                <!-- Fin Cuerpo -->
            </table>
            <!-- Fin Tabla -->
        </div>
    </div>
    <!-- fin container -->

    <!-- MODAL DELETE -->
    <div class="modal fade" id="ModalDeleteZonaComun" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                        <div class="col-xl-10 col-lg-10 col-md-10">
                            <div class="form-group">
                                <p>¿Está seguro que desea eliminar la zona común?</p>
                            </div>
                            
                            <input type="text" class="form-control" name="TokenDel" id="TokenDel" hidden>
                                
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="EliminarZonaComun();">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Fin wrapper -->
<?php $this->load->view('footer'); ?>

<script type="text/javascript">

    function LanzarModal(ID){

        document.getElementById("TokenDel").value = ID;
        $('#ModalDeleteZonaComun').modal('show');
    }


    function EliminarZonaComun(){

        var TokenDel = document.getElementById("TokenDel");

        var Registro = {
            "Res_Token": TokenDel.value
        };

        $.ajax({
            url: '<? echo base_url('ZonaComun/borrar_zonacomun/');?>',
            method: 'POST',
            data: Registro,
            success: function(response)
            {
                
                $('#ModalDeleteZonaComun').modal('hide');
                window.location = '/uniger/zonacomun/Listar/delete/';

            },
            error: function (err) {
                alert("Ocurrió un error: " + JSON.stringify(err, null, 2));
            }
        });
        }

</script>


<?php 
    if ($this->uri->segment(3)== "success")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Zona comun creada correctamente");
            });
        </script>

<?php } else if ($this->uri->segment(3)== "delete")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Zona comun eliminada correctamente");
            });
        </script>   

<?php } else if ($this->uri->segment(3)== "update")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Zona comun actualizada correctamente");
            });
        </script>
<?php } ?>