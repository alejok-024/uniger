<?php $this->load->view('header'); ?>
    

        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                                    <li class="breadcrumb-item"><a href="#">Recibos</a></li>
                                    <li class="breadcrumb-item"><a href="#">Cuota Extraordinaria</a></li>
                                    <li class="breadcrumb-item active">Reportes</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Reportes</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Promedio cuotas extraordinarias</h4>
                                <p class="text-muted m-b-30 font-14">(Esta gráfica representa el promedio de las cuotas extraordinarias que se pagaron por apartamento)</p>
                                <div id="grafica1"></div>
                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-lg-6">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Multas por inmuebles</h4>
                                <p class="text-muted m-b-30 font-14">(Esta gráfica representa la cantidad de multas generadas en un inmueble)</p>
                                <div id="grafica2"></div>
                            </div>
                        </div>
                    </div>

                
                    

                </div>  
            </div> 
        </div>
        <!-- end wrapper -->
        <?php $this->load->view('footer'); ?>

        <!--C3 Chart-->
        <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/plugins/d3/d3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('application/views/'); ?>assets/plugins/c3/c3.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/pages/c3-chart-init.js"></script> 

<script>
    ! function(e) {
        "use strict";
        var a = function() {};
        a.prototype.init = function() {
            c3.generate({
                bindto: "#grafica1",
                data: {
                    columns: [
                      
                        <?php 
                            foreach ($promedios as $promedio) {?>
                               ["<?php echo $promedio->Cuo_Ext_Id; ?>",<?php echo $promedio->valor; ?> ],
                        <?php } 
                        ?>
                    ],
                    type: "pie"
                },
                color: {
                    pattern: ['#1f77b4', '#69f0ae']
                },
                bar: {
                    width: {
                        ratio: 0.2 
                    }
                }
            })
        }, e.ChartC3 = new a, e.ChartC3.Constructor = a
    }(window.jQuery),
    function(e) {
        "use strict";
        e.ChartC3.init()
    }(window.jQuery);
</script>

<script>
    ! function(e) {
        "use strict";
        var a = function() {};
        a.prototype.init = function() {
            c3.generate({
                bindto: "#grafica2",
                data: {
                    columns: [
                        <?php 
                            foreach ($inmuebles as $inmueble) {?>
                               ["<?php echo $inmueble->Inm_Id; ?>",<?php echo $inmueble->Mul_Valor; ?> ],
                        <?php } ?>
                    ],
                    type: "bar"
                },
                color: {
                    pattern: ['#1f77b4', '#69f0ae']
                },
                bar: {
                    width: {
                        ratio: 0.2 
                    }
                }
            })
        }, e.ChartC3 = new a, e.ChartC3.Constructor = a
    }(window.jQuery),
    function(e) {
        "use strict";
        e.ChartC3.init()
    }(window.jQuery);
</script>

