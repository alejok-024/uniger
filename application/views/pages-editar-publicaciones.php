<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="pages-inicio-comunicaciones.html">Comunicaciones</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <a href="pages-publicaciones.html">Publicaciones</a>
                                </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Editar publicación</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            <div class="modal bs-example-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="card-header">
                            <h3>Editar publicación</h3>
                        </div>
                        <div class="modal-body">
                        <?php echo form_open('Publicaciones/Editar'); ?>
                            <div class="form-group">
                                <label for="Pub_Contenido">Contenido de la publicación</label>
                                <textarea class="form-control" id="Pub_Contenido" rows="3" name="Pub_Contenido" value= "<?php echo $informacion->Pub_Contenido; ?>" placeholder="Ingrese texto..."></textarea>
                                </textarea>
                            </div>  
                            </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Publicar</button>
                        </div>
                        <input type="hidden" name="publicacion_id" value="<?php echo $id; ?>">
                        <?php echo form_close(); ?>
                    </div>

                </div>
            </div>

            <?php $this->load->view('footer'); ?>