<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- Contenedor -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                            <a href="<?php echo base_url('Inmuebles'); ?>">Inmuebles</a>                           
                            </li>
                            <li class="breadcrumb-item active">
                            <a href="<?php echo base_url('Inmuebles/listar'); ?>">Consultar inmueble</a>
                            </li>
                        </ol>
                    </div>
                    <h2 class="page-title">Consultar usuarios</h2>
                </div>
            </div>
        </div>
       <!-- Fin titulo y mida de pan -->

        <!-- Resultados -->
        <!-- Contenido Principal -->
        <div class="row">

            <!-- Tabla -->

            <table class="table table-striped" id="datatable">

                <!-- Encabezado -->
                <thead>
                    <tr>
                        <div class="card-header">

                            <form>
                                <h6>
                                    <i class="dripicons-view-list"></i>
                                    &nbsp; Información inmueble &nbsp;
                                    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg">Agregar usuario</button>

                                </h6>
                            </form>

                        </div>
                        <th>Usuarios</th>
                        <th>Rol</th>
                        <th>Acciones</th>
                    </tr>

                </thead>
                <!-- Fin Encabezado -->
                <!-- Cuerpo -->
                <tbody>

                    <?php
                         foreach($inmuebles as $inmueble){

                    ?>
                    <tr>

                        <td>
                            <?php echo $inmueble->Usu_Nombres." ".$inmueble->Usu_Apellidos?> </td>
                        <td>
                            <?php echo $inmueble->InmXUsu_Rol?>
                        </td>
                        <td>
                            <?php if ($inmueble->InmXUsu_Rol != 'Dueño') { ?>
                        <a href="<?php echo base_url("Inmuebles/borrar_usuario/".$inmueble->Usu_Id.'/'.$inmueble->Inm_Id.'/'.$inmueble->InmXUsu_Rol);?>" class="btn btn-outline-danger waves-effect waves-light mr-2 borrarUsuInm"><i class=" mdi mdi-delete"></i></a> 
                        <?php } ?>
                            <a href="<?php echo base_url("Inmuebles/Actulizar_usu/".$inmueble->Usu_Id.'/'.$inmueble->Inm_Id);?>" class="btn btn-primary waves-effect waves-light"
                                data-toggle="modal" data-target=".bs-example-modal-lg1">
                                <i class="ion-edit"></i>
                            </a>

                        </td>
                    </tr>
                    <?php }?>
                </tbody>
                <!-- Fin Cuerpo -->
            </table>
            <!-- Fin Tabla -->
        </div>
    </div>
    <!-- fin container -->
</div>
<!-- Fin wrapper -->
<!-- Modal Agregar usuario -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"
    style="display: center;">
    <?php echo form_open('inmuebles/agregar_usu');?>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Agregar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <!-- Tabla -->
                <div class="col-md-4 mb-4">
                    <label>Usuario</label>
                    <input type="hidden" value="<?php echo $inmueble->Inm_Id;?>" name="Inm_Id" required>

                    <select class="selectpicker col-12 d-block" data-live-search="true" name="Usu_Id" required>
                        <option value="">Seleccione</option>
                        <?php foreach($usuarios as $usuario): ?>
                        <option data-tokens="<?php echo $usuario->Usu_Nombres." ".$usuario->Usu_Apellidos;?>" value="<?php echo $usuario->Usu_Id;?>">
                            <?php echo $usuario->Usu_Nombres." ".$usuario->Usu_Apellidos;?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                    <datalist id="browsers" required>
                        <option value="">Seleccione</option>
                        <?php foreach($usuarios as $usuario): ?>
                        <?php echo $usuario->Usu_Nombres." ".$usuario->Usu_Apellidos;?>
                        </option>
                        <?php endforeach; ?>
                    </datalist>
                </div>

                <div class="col-md-4 mb-4">
                    <label>Rol </label>
                    <select name="InmXUsu_Rol" id="" class="form-control" required="">
                        <option value="Arrendatario">Arrendatario</option>
                        <option value="Inquilino">Inquilino</option>
                        <option value="Comodatario">Comodatario</option>
                        <option value="Albacea">Albacea</option>
                    </select>
                    <br>
                    <div>
                        <button type="submit" class="btn btn-primary" onclick="form.submit();this.disabled=true">Registrar</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<?php echo form_close();?>
</div>
<!-- end modal -->
<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"
    style="display: none;">
    <?php echo form_open('inmuebles/Actulizar_usu');?>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Actualizar usuario</h5>
                <input type="hidden" value="<?php echo $inmueble->Inm_Id;?>" name="Inm_Id">
                <input type="hidden" value="<?php echo $inmueble->Usu_Id;?>" name="Usu_Id">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <!-- Tabla -->
                <div class="col-md-4 mb-4">
                    <label>Rol </label>
                    <input type="hidden" value="<?php echo $inmueble->Inm_Id;?>" name="InmXUsu_Rol">
                    <select class="form-control" id="" name="InmXUsu_Rol">
                        <?php
                                     if($inmueble->Inm_Id == "Arrendatario"){?>
                                    <option value="Inquilino">Inquilino</option>
                                    <option value="Comodatario">Comodatario</option>
                                    <option value="Albacea">Albacea</option>
                                    <?php
                                    }elseif($inmueble->Inm_Id == "Inquilino"){?>
                                    <option value="Arrendatario">Arrendatario</option>
                                    <option value="Comodatario">Comodatario</option>
                                    <option value="Albacea">Albacea</option>
                                    <?php
                                    }elseif($inmueble->Inm_Id == "Comodatario"){?>
                                    <option value="Arrendatario">Arrendatario</option>
                                    <option value="Inquilino">Inquilino</option>
                                    <option value="Albacea">Albacea</option>
                                    <?php 
                                         }else{
                                        ?>
                                    <option value="Albacea">Albacea</option>
                                    <option value="Comodatario">Comodatario</option>
                                    <option value="Arrendatario">Arrendatario</option>
                                    <option value="Inquilino">Inquilino</option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('Inm_Tipo'); ?>
                    </select>
                    <br>
                    <div>
                        <button type="submit" class="btn btn-primary" onclick="form.submit();this.disabled=true">Cambiar</button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<?php $this->load->view('footer'); ?>