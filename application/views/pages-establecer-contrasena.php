<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Recuperar Contraseña | UNIGER</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Themesdesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Iconos -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Css -->
    <link href="<?php echo base_url('application/views/'); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/style.css" rel="stylesheet" type="text/css" />

</head>


    <body>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center mt-0 m-b-15">
                    <a href="index.html" class="logo logo-admin"><img src="<?php echo base_url('application/views/'); ?>assets/images/logo-dark.png" height="30" alt="logo"></a>                    </h3>

                    <h4 class="text-muted text-center font-18"><b>Recuperar Contraseña</b></h4>
                    <?php  
                    if ($this->session->flashdata('error')) {
                        echo $this->session->flashdata('error'); 
                    }
                    ?>
                    <div class="p-3">
                    <?php $args = array('class' => 'form-horizontal m-t-20') ?>
                        <?php echo form_open('Gestion/establecerContrasena', $args); ?>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" type="password" name="contra_uno" required="" placeholder="Ingrese su contraseña">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" type="password" name="contra_dos" required="" placeholder="Repita la contraseña">
                                    <input type="hidden" name="email" value="<?php echo $this->uri->segment(3); ?>">
                                    <input type="hidden" name="token" value="<?php echo $this->uri->segment(4); ?>">

                                </div>
                            </div>

                            <div class="form-group text-center row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-info btn-block waves-effect waves-light" type="submit">Restablecer contraseña</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>




        <!-- jQuery  -->
        <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/modernizr.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.scrollTo.min.js"></script>


        <!-- Javascript -->
        <script src="<?php echo base_url('application/views/'); ?>assets/js/app.js"></script>

    </body>
</html>