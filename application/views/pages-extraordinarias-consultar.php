<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('Presupuestos'); ?>">Inicio cuotas extraordinarias</a>
                                </li>
                                <li class="breadcrumb-item active">Consulta de cuotas extraordinarias</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Cuotas extraordinarias</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            
            <div class="row d-flex content-justified-center">
                <div class="col-md-8 offset-md-2">
                    <ul class="nav nav-pills nav-justified" role="tablist">
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link active show" data-toggle="tab" href="#General" role="tab" aria-selected="true">General</a>
                        </li>
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" data-toggle="tab" href="#Detalle" role="tab" aria-selected="false">Detalle</a>
                        </li>
                    </ul>

                </div>
            </div>

            <div class="row">
                <div class="col-md-10 offset-md-1">
                    
                <div class="tab-content">
                        <div class="tab-pane p-3 active show" id="General" role="tabpanel">
                            <table class="table table-striped" id="generalTable">
                                <thead>
                                <tr>
                                    <th>Periodo</th>
                                    <th>Mes</th>
                                    <th>Valor</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($cuotas_extraordinarias as $cuota) { ?>
                                    <tr id="row-<?php echo $cuota->Cuo_Ext_Id; ?>">
                                        <td><?php echo $cuota->Per_Periodo; ?></td>
                                        <td><?php echo $cuota->Mes_Nombre; ?></td>
                                        <td><?php echo number_format($cuota->Cuo_Ext_Valor); ?></td>
                                        <td><?php echo $cuota->Cuo_Ext_Descripcion; ?></td>
                                        <td>
                                            <a href="#" class="btn btn-outline-danger waves-effect waves-light mr-2 borrarPresupuesto" id="pre-<?php echo $cuota->Cuo_Ext_Id; ?>"><i class=" mdi mdi-delete"></i></a>
                                            <button class="btn btn-outline-danger waves-effect waves-light mr-2 d-none" disabled id="delete-<?php echo $cuota->Cuo_Ext_Id; ?>"><div class="loader"></div></button>

                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane p-3" id="Detalle" role="tabpanel">
                            
                            <table class="table table-striped" id="datatable">
                                <thead>
                                <tr>
                                    <th>Periodo</th>
                                    <th>Mes</th>
                                    <th>Número Inmueble</th>
                                    <th>Tipo</th>
                                    <th>Valor</th>
                                    <th>Descripción</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($detalle_cuotas_extraordinarias as $detalle_cuota) { ?>
                                    <tr>
                                        <td><?php echo $detalle_cuota->Per_Periodo; ?></td>
                                        <td><?php echo $detalle_cuota->Mes_Nombre; ?></th>
                                        <td><?php echo $detalle_cuota->Inm_Id; ?></th>
                                        <td><?php echo $detalle_cuota->Inm_Tipo; ?></th>
                                        <td><?php echo number_format($detalle_cuota->Det_Cuo_Ext_Valor); ?></td>
                                        <td><?php echo $detalle_cuota->Det_Cuo_Ext_Descripcion; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div><!-- Fin container -->
    </div>
    <script>
        var base_url = "<?php echo base_url(); ?>"
    </script>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>
    <?php 
    if ($this->uri->segment(3)== "success")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Cuota creada correctamente");
            });
        </script>
    <?php } ?>