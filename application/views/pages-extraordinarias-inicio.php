<?php $this->load->view('header'); ?>
    <!-- Fin MENU -->

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="inicio">Inicio</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="recibos">Recibos</a>
                                </li>
                                <li class="breadcrumb-item active">Cuotas extraordinarias</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Cuotas extraordinarias</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->


            <div class="row d-fluid justify-content-center">
                <!-- Item -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<? echo base_url('Cuotas_Extraordinarias/crear');?>">
                                <div class="card-icon">
                                    <i class="mdi mdi-note-plus "></i>
                                </div>
                                <div class="card-text">

                                    <p class="card-text">Crear</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Item -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<? echo base_url('Cuotas_Extraordinarias/consultar');?>">
                                <div class="card-icon">
                                    <i class="mdi mdi-magnify"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Consultar</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- Item -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                            <a href="<? echo base_url('Cuotas_Extraordinarias/reportes');?>">
                                <div class="card-icon">
                                    <i class="mdi mdi-chart-bar"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Reportes</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
        

    </div>
    <!-- Fin wrapper -->

    <!-- Footer -->
    <?php $this->load->view('footer'); ?>