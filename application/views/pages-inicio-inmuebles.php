<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="pages-inicio-inmuebles.html">Inmuebles</a>
                                </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Inmuebles</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            <div class="row">
                <!-- Registro inmueble -->
                <div class="col-lg-3 offset-md-1 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                        <a href="<? echo base_url('/inmuebles/crear');?>">
                                <div class="card-icon">
                                    <i class="fa fa-building-o"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Registro inmuebles</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- consultar -->
                <div class="col-lg-4 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                        <a href="<? echo base_url('/inmuebles/listar');?>">
                                <div class="card-icon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Consultar inmuebles</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Reportes -->
                <div class="col-lg-3 center-align">
                    <div class="card m-b-30 text-white bg-gray">
                        <div class="card-body">
                        <a href="<? echo base_url('/inmuebles/reportes');?>">
                                <div class="card-icon">
                                    <i class="mdi mdi-chart-bar"></i>
                                </div>
                                <div class="card-text">
                                    <p class="card-text">Reportes inmuebles</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin container -->
    </div>
    <!-- Fin wrapper -->


<?php $this->load->view('footer'); ?>