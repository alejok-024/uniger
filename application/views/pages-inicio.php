<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            <div class="row">
                <div class="col-md-12">
                    <img id="imagenPrincipal" src="<?php echo base_url('application/views/assets/images/logo-dark.png');?>" alt="Uniger" class="img-fluid animated bounce delay-2s">
                </div>
            </div>
        </div><!-- Fin container -->
    </div>
    <!-- Fin wrapper -->
    
    <!--  Modal para completar la información del perfil-->
    <div class="modal in" id="completaPerfil" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Completa tu perfil</h5>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <?php echo form_open('Usuarios/completarInfo'); ?>
                            <div class="row">
                                <div class="offset-md-1 col-md-10">
                                <?php  
                                if ($this->session->flashdata('error')) {
                                    echo "<p class='text-danger'>¡Verifique la información ingresada¡</p>";
                                }
                                ?>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label">Tipo de documento</label>
                                        <div class="col-sm-6">
                                            <select name="tipo_documento" class="form-control" id="">
                                                <option value="">Seleccione</option>
                                                <option value="CC">Cédula de ciudadania</option>
                                                <option value="TI">TI</option>
                                                <option value="visa">visa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label">Número de documento</label>
                                        <div class="col-sm-6">
                                            <input class="form-control" name="numero_documento" type="number" id="example-text-input">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label">Fecha de nacimiento</label>
                                        <div class="col-sm-6">
                                            <input class="form-control" name="fecha_nacimiento" type="date" id="example-text-input">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="form-group row">
                                <button type="submit" class="offset-sm-4 col-sm-4 btn btn-success btn-lg">Actualizar</button>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <?php print($this->session->flashdata('incompleto')); ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"></script>
    <?php $this->load->view('footer'); ?>

    <?php if($this->session->flashdata('incompleto')){ ?>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#completaPerfil').modal({
                    show: true,
                    backdrop: 'static',
                    keyboard: false
                });
            });
        </script>
    <?php } ?>