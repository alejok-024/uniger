<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('multas'); ?>">Inicio Multas</a>
                                </li>
                                <li class="breadcrumb-item active">Multas</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Multas</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->

            <div class="row">
                <div class="col-md-10 offset-md-1">

                    <table class="table table-striped" id="generalTable">
                        <thead>
                            <tr>
                                <th>Periodo</th>
                                <th>Mes facturación</th>
                                <th>Fecha incidente</th>
                                <th>Inmueble</th>
                                <th>Pagador</th>
                                <th>Valor</th>
                                <th>Descripción</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($multas as $multa) { ?>
                                <tr id="row-<?php echo $multa->Mul_Id; ?>">
                                    <td>
                                        <?php echo $multa->Per_Periodo; ?>
                                    </td>
                                    <td>
                                        <?php echo $multa->Mes_Nombre; ?>
                                    </td>
                                    <td>
                                        <?php echo $multa->Mul_Fecha; ?>
                                    </td>
                                    <td>
                                        <?php echo $multa->Inm_Id; ?>
                                    </td>
                                    <td>
                                        <?php echo $multa->Inm_Id; ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($multa->Mul_Valor); ?>
                                    </td>
                                    <td>
                                        <?php echo $multa->Mul_Descripcion; ?>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-outline-danger waves-effect waves-light mr-2 borrarMulta" id="pre-<?php echo $multa->Mul_Id; ?>"><i class=" mdi mdi-delete"></i></a>
                                        <button class="btn btn-outline-danger waves-effect waves-light mr-2 d-none" disabled id="delete-<?php echo $multa->Mul_Id; ?>">
                                            <div class="loader"></div>
                                        </button>

                                    </td>
                                </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Fin container -->
    </div>
    <script>
        var base_url = "<?php echo base_url(); ?>"
    </script>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>
    <?php 
    if ($this->uri->segment(3)== "success")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Multa creada correctamente");
            });
        </script>
    <?php } ?>