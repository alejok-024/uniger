<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('multas'); ?>">Inicio Multas</a>
                                </li>
                                <li class="breadcrumb-item active">Multas</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Crear</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            
            <div class="row">

                    <!-- Col -->
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Multas</h4>
                                <p class="text-muted m-b-30 font-14">Creación de multa.</p>
                                <?php $args= array('id' => 'crear_multa'); ?>
                                <?php echo form_open('Multas/guardar', $args); ?>

                                    <div class="form-group">
                                        <label for="input-multa">Valor multa (COP)*</label>
                                        <input class="form-control input-val" name="input-valor" id="input-valor" value="<?php echo set_value('input-valor'); ?>" required">
                                        <?php echo form_error('input-valor'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-fecha">Fecha*</label>
                                        <div>
                                            <input type="date" class="form-control" name="input-fecha" id="input-fecha" value="<?php echo set_value('input-fecha'); ?>">   
                                        </div>
                                        <?php echo form_error('input-fecha'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-inmueble">Inmueble*</label>
                                        <div>
                                            <select class="selectpicker col-lg-12" name="input-inmueble" id="input-inmueble" data-live-search="true" title="Seleccione un inmueble">
                                                <?php foreach ($inmuebles as $inmueble) { ?>
                                                    <option data-tokens="<?php echo $inmueble->Inm_Id; ?>"><?php echo $inmueble->Inm_Id; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php echo form_error('input-inmueble'); ?>
                                    </div>
										
									<!-- 
										1 = Paga el dueño
										2 = Paga el inquilinos
									-->
                                    <div class="form-group">
										<label>¿Quien pagará esta multa?</label>
										<label><input type="radio" name="input-pagador" value="1" required>Dueño</label> 
            							<label><input type="radio" name="input-pagador" value="0" required>Inquilino</label>
                                        <?php echo form_error('input-pagador'); ?>
                                    </div>

                                    <div class="form-group">
										<label>¿Se encuentra registrado el infractor en la plataforma?</label>
										<label><input type="radio" name="input-usuario-registrado" value="Si" required>Si</label> 
            							<label><input type="radio" name="input-usuario-registrado" value="No" required>No</label>
                                        <?php echo form_error('input-usuario-registrado'); ?>
                                    </div>

                                    <div class="form-group d-none" id="loConoce">
                                        <label for="input-usuario-id">Usuario infractor*</label>
                                            <div>
                                                <select class="form-control" name="input-usuario-id" id="input-usuario-id" data-live-search="true" title="Seleccione un periodo">
													<option value="">Seleccione</option>
                                                    <?php foreach ($usuarios as $usuario) { ?>
                                                        <option value="<?php echo $usuario->Usu_Id; ?>"><?php echo $usuario->Nombres; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <?php echo form_error('input-usuario-id'); ?>
                                    </div>

                                    <div class="form-group d-none" id="noLoConoce">
                                        <label for="input-usuario">Usuario infractor*</label>
                                            <div>
                                                <input type="text" class="form-control" name="input-usuario" placeholder="Escriba el nombre de la persona" value="<?php echo set_value('input-usuario'); ?>">
                                            </div> 
                                            <?php echo form_error('input-usuario'); ?>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label for="input-descripcion">Causal de la multa*</label>
                                        <textarea name="input-descripcion" id="input-descripcion" class="form-control" rows="5" placeholder="Describa el motivo de la multa"><?php echo set_value('input-descripcion'); ?></textarea>
                                        <?php echo form_error('input-descripcion'); ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-success " id="submitMulta">Registrar multa</button>

                                        <button class="btn btn-success d-none" disabled id="loaderMulta"><span class="mr-2">Creando, por favor espere...</span><div class="loader"></div></button>
                                    </div>

                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>


        </div><!-- Fin container -->
    </div>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>