<?php $this->load->view('header'); ?> 
<!-- Titulo Página -->
<div class="row">
   <div class="col-sm-12">
      <div class="page-title-box">
         <div class="btn-group pull-right">
            <ol class="breadcrumb hide-phone p-0 m-0">
               <li class="breadcrumb-item"><a href="#">Upcube</a></li>
               <li class="breadcrumb-item"><a href="#">Pages</a></li>
               <li class="breadcrumb-item active">Starter</li>
            </ol>
         </div>
         <h4 class="page-title">Starter</h4>
      </div>
   </div>
</div>
<!-- Fin titulo pagina y miga de pan -->
<div class="row">
   <div class="container">
      <div class="row">
         <div class="col-md-12  toppad  offset-md-0 ">
            <a href="edit.html" class="float-right" >Cerrar sesion</a>
         </div>
         <div class="col-md-6  offset-md-0  bt-5 mt-5" >
            <div class="card">
               <div class="card-body">
                  <h2 class="card-title"><?php echo $datos->Usu_Nombres. " ". $datos->Usu_Apellidos; ?></h2>
                  <table class="table table-user-information ">
                     <tbody>
                     <?php echo form_open_multipart('perfil/actualizarInfoUsuario');  ?>
                        <tr>
                            <td>
                                 <?php  
                                    $url = $_SERVER['DOCUMENT_ROOT'] ."/uniger/application/views/imgPerfil/perfil".$this->session->userdata('Usu_Id').".jpg";
                                    if (file_exists($url)) {
                                          $url =  base_url("application/views/imgPerfil/perfil".$this->session->userdata('Usu_Id')).".jpg";
                                    }
                                    else
                                    {
                                          $url = base_url("application/views/assets/images/user-placeholder.png");
                                    }
                                ?>
                               <img src="<?php echo $url; ?>" alt="user-img" class="img-fluid rounded-circle" />
                            </td>
                            <td>
                                <input class="btn btn-default" type="file" accept="image/png, image/jpeg, image/gif" name="foto_Perfil"/>
                            </td>
                        </tr>
                        <tr>
                           <td>Email:</td>
                           <td><?php echo $datos->Usu_Email; ?></td>
                        </tr>
                        <tr>
                           <td>Tipo de documento:</td>
                           <td><?php echo $datos->Usu_Apellidos; ?></td>
                        </tr>
                        <tr>
                           <td>Número de documento:</td>
                           <td><?php echo $datos->Usu_Numero_Documento; ?></td>
                        </tr>
                        <tr>
                           <td>Fecha de nacimiento:</td>
                           <td><?php echo $datos->Usu_Fecha_Nacimiento; ?></td>
                        </tr>
                        <tr>
                           <td>Fecha de ingreso:</td>
                           <td><?php echo $datos->Usu_Fecha_Registro; ?></td>
                        </tr>
                        <tr>
                           <td>Teléfono celular:</td>
                           <td>
                              <input
                                 name="telefonoCel"
                                 type="number"
                                 maxLength="5"
                                 value=<?php echo $datos->Usu_Telefono_Celular; ?>
                                 focus
                                 class="form-control"
                                 />
                                 <?php echo form_error('telefonoCel'); ?>
                           </td>
                        </tr>
                        <tr>
                           <td>Teléfono fijo:</td>
                           <td>
                              <input
                                 name="telefonoFijo"
                                 type="number"
                                 maxLength="5"
                                 value=<?php echo $datos->Usu_Telefono_Fijo; ?>
                                 focus
                                 class="form-control"
                                 />
                                 <?php echo form_error('telefonoFijo'); ?>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <button type="submit" class="btn btn-primary ml-2" id="updateInfo">Guardar cambios</a>
                  <?php echo form_close(); ?>
               </div>
            </div>
         </div>

         <div class="col-md-6  offset-md-0  bt-5 mt-5" ">
            <div class="card">
               <div class="card-body">
                  <h3 class="card-title">Cambiar clave</h3>
                    <?php echo form_open('perfil/actualizarContrasena'); ?>
                    <table class="table table-user-information ">
                     <tbody>
                        <tr>
                           <td>Clave actual:</td>
                           <td>
                               <input
                               icon="password-icon"
                               name="password"
                               type="password"
                               value=''
                               focus
                               class="form-control"
                               />
                               <?php echo form_error('password'); ?>
                           </td>
                        </tr>
                        <tr>
                           <td>Clave nueva:</td>
                           <td>
                              <Input
                                 icon="password-icon"
                                 name="newPassword"
                                 type="password"
                                 value=''
                                 class="form-control"
                                 />
                                 <?php echo form_error('newPassword'); ?>
                           </td>
                        </tr>
                        <tr>
                           <td>Repita clave nueva:</td>
                           <td>
                              <Input
                                 icon="password-icon"
                                 name="newPasswordRepeat"
                                 type="password"
                                 value=''
                                 class="form-control"
                                 />
                                 <?php echo form_error('newPasswordRepeat'); ?>
                           </td>
                        </tr>
                     </tbody>
                    </table>
                    <button class="btn btn-primary ml-2" id="updatePassword">Cambiar clave</button>
                    <?php echo form_close(); ?> 
                </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('footer'); ?>
<?php 
    $success = $this->session->flashdata('success');
    if ($success) 
    {
    ?>
    <script>
        $(document).ready(function(){
            alertify.success(<?php echo $success; ?>);
        });
    </script>   
    <?php
    }
?>

