<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('Presupuestos'); ?>">Inicio presupuestos</a>
                                </li>
                                <li class="breadcrumb-item active">Presupuestos</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Presupuestos</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            
            <div class="row d-flex content-justified-center">
                <div class="col-md-8 offset-md-2">
                    <ul class="nav nav-pills nav-justified" role="tablist">
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link active show" data-toggle="tab" href="#General" role="tab" aria-selected="true">General</a>
                        </li>
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" data-toggle="tab" href="#Detalle" role="tab" aria-selected="false">Detalle</a>
                        </li>
                    </ul>

                </div>
            </div>

            <div class="row">
                <div class="col-md-10 offset-md-1">
                    
                <div class="tab-content">
                        <div class="tab-pane p-3 active show" id="General" role="tabpanel">
                            <table class="table table-striped" id="generalTable">
                                <thead>
                                <tr>
                                    <th>Periodo</th>
                                    <th>Valor</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($presupuestos as $presupuesto) { ?>
                                    <tr id="row-<?php echo $presupuesto->Pre_Id; ?>">
                                        <th scope="row"><?php echo $presupuesto->Per_Periodo; ?></th>
                                        <td><?php echo number_format($presupuesto->Pre_Valor); ?></td>
                                        <td><?php echo $presupuesto->Pre_Descripcion; ?></td>
                                        <td>
                                            <a href="#" class="btn btn-outline-danger waves-effect waves-light mr-2 borrarPresupuesto" id="pre-<?php echo $presupuesto->Pre_Id; ?>"><i class=" mdi mdi-delete"></i></a>
                                            <button class="btn btn-outline-danger waves-effect waves-light mr-2 d-none" disabled id="delete-<?php echo $presupuesto->Pre_Id; ?>"><div class="loader"></div></button>

                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane p-3" id="Detalle" role="tabpanel">
                            
                            <table class="table table-striped" id="datatable">
                                <thead>
                                <tr>
                                    <th>Número Inmueble</th>
                                    <th>Tipo</th>
                                    <th>Valor</th>
                                    <th>Periodo</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($detalle_presupuestos as $detalle_presupuesto) { ?>
                                    <tr>
                                        <th scope="row"><?php echo $detalle_presupuesto->Inm_Id; ?></th>
                                        <th scope="row"><?php echo $detalle_presupuesto->Inm_Tipo; ?></th>
                                        <td><?php echo number_format($detalle_presupuesto->Det_Pre_Valor); ?></td>
                                        <td><?php echo $detalle_presupuesto->Per_Periodo; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div><!-- Fin container -->
    </div>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>
    <?php 
    if ($this->uri->segment(3)== "success")
    {?>
        <script>
            $(document).ready(function () {
                alertify.success("Presupuesto creado correctamente");
            });
        </script>
    <?php } ?>