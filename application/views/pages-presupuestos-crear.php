<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('Presupuestos'); ?>">Inicio presupuestos</a>
                                </li>
                                <li class="breadcrumb-item active">Presupuestos</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Presupuestos</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            
            <div class="row">

                    <!-- Col -->
                    <div class="col-lg-6 offset-lg-3">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Administración</h4>
                                <p class="text-muted m-b-30 font-14">Gestión del presupuestos anuales.</p>
                                <?php $args= array('id' => 'crear_presupuesto'); ?>
                                <?php echo form_open('Presupuestos/guardar', $args); ?>
                                    <div class="form-group">
                                        <label for="input-presupuesto">Valor presupuesto (COP)*</label>
                                        <input class="form-control input-val" name="input-presupuesto" id="input-presupuesto" required value="<?php echo set_value('input-presupuesto'); ?>">
                                        <?php echo form_error('input-presupuesto'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-periodo">Periodo*</label>
                                        <div>
                                        <select class="selectpicker col-lg-12" name="input-periodo" id="input-periodo" data-live-search="true" title="Seleccione un periodo">
                                            <?php foreach ($periodos_presupuesto as $periodo) { ?>
                                                <option data-tokens="<?php echo $periodo->Per_Periodo; ?>"><?php echo $periodo->Per_Periodo; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                        <?php echo form_error('input-periodo'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-descripcion">Descripción presupuesto*</label>
                                        <textarea name="input-descripcion" id="input-descripcion" class="form-control" rows="5" placeholder="Describa la razón de la cuota extraordinaria"><?php echo set_value('input-descripcion'); ?></textarea>
                                        <?php echo form_error('input-descripcion'); ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-success " id="submitBtn">Crear presupuesto</button>

                                        <button class="btn btn-success d-none" disabled id="loader"><span class="mr-2">Creando, por favor espere...</span><div class="loader"></div></button>
                                    </div>

                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>


        </div><!-- Fin container -->
    </div>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>