<?php $this->load->view('header'); ?>

<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Comunicaciones</a>
                            </li>
                            <li class="breadcrumb-item active">Publicaciones</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Publicaciones</h4>
                </div>
            </div>
        </div>
        <br>
        <!--Publicaciones-->
        <?php
                         foreach($publicaciones as $publicacion){

                    ?>

        <div class="card m-b-20">
            <div class="card-body">

                <h4>
                    <i class="fa fa-calendar-check-o"></i>
                    <?php echo $publicacion->Pub_Fecha_Publicacion?> 
                    <a href="<?php echo base_url("Publicaciones/Editar/".$publicacion->Pub_Id);?>" class="btn btn-outline-info waves-effect waves-light mr-2">
                        <i class="ion-edit"></i>
                    </a>
                    <a href="<?php echo base_url("Publicaciones/Borrar/".$publicacion->Pub_Id);?>" class="btn btn-outline-danger waves-effect waves-light mr-2">
                        <i class=" mdi mdi-delete"></i>
                    </a>
                </h4>
                <hr>
                <p>
                    <h5>Descripción</h5>
                    <?php echo $publicacion->Pub_Contenido?>
                </p>
                <hr>
                <label>Tipo información:
                    <?php echo $publicacion->Pub_Tipo_Informacion?>
                </label>
                <br>
                <?php echo form_open_multipart('publicaciones/Agregar_publicacion');  ?>
                <td>
                                 <?php  
                                    $url = $_SERVER['DOCUMENT_ROOT'] ."/uniger/application/views/Publicaciones/Publicaciones".$this->session->userdata('Usu_Id').".jpg";
                                    if (file_exists($url)) {
                                          $url =  base_url("application/views/Publicaciones/Publicaciones".$this->session->userdata('Usu_Id')).".jpg";
                                    }
                                    else
                                    {
                                          $url = base_url("application/views/assets/images/user-placeholder.png");
                                    }
                                ?>
                               <img src="<?php echo $url; ?>" alt="user-img" class="img-fluid rounded-circle" />
                            </td>
                <hr>
                <a class="btn btn-outline-success waves-effect waves-light mr-2">
                    <i class="mdi mdi-check"></i>
                </a>
                <a class="btn btn-outline-info waves-effect waves-light mr-2">
                    <i class="mdi mdi-comment-plus-outline"></i>
                </a>

            </div>

        </div>
        <?php }?>
        <div>
            <a href="<?php echo base_url("Publicaciones/Agregar_publicacion ");?>" class="btn btn-default">
                <i class="fa fa-plus"></i></a>
        </div>
        
    </div>
    
</div>



    <?php $this->load->view('footer'); ?>