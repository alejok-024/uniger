<?php $this->load->view('header'); ?>

    <!-- wrapper -->
    <div class="wrapper">
        <!-- container -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row ">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('recibos'); ?>">Inicio recibos de pago</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url('multas'); ?>">Inicio Multas</a>
                                </li>
                                <li class="breadcrumb-item active">Multas</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Multas</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->

            <div class="row">
                <div class="col-md-10 offset-md-1">

                    <table class="table table-striped" id="generalTable">
                        <thead>
                            <tr>
                                <th>Inmueble</th>
                                <th>Tipo Inmueble</th>
                                <th>Fecha recibo de pago</th>
                                <th># Recibo de pago</th>
                                <th>Descargar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($recibos as $recbos) { ?>
                                <tr id="row-<?php echo $recbos->Inm_Id; ?>">
                                    <td>
                                        <?php echo $recbos->Inm_Tipo; ?>
                                    </td>
                                    <td>
                                        <?php echo "pendiente"; ?>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <?php echo $recbos->Rec_Pag_Numero; ?>
                                    </td>
                                    <td>
                                        <?php $pdf =  base_url("/application/controllers/recibos/".$recbos->Rec_Pag_Pdf); ?>
                                        <a class="btn btn-outline-danger waves-effect waves-light mr-2" href="<?php echo $pdf; ?>" download><i class="mdi mdi-file-pdf "></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Fin container -->
    </div>
    <script>
        var base_url = "<?php echo base_url(); ?>"
    </script>
    <!-- Fin wrapper -->
    <?php $this->load->view('footer'); ?>