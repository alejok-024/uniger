<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item active">Registro</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Mantenimiento</h4>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <br>
                    <div class="card m-b-30">
                        <div class="card-header">
                                <h3>Registro de mantenimiento</h3>
                                <a>Los campos marcados con * son olbligatorios</a>
                        </div>
                        <div class="card-body">
                        <?php echo form_open('Mantenimiento/crear_mantenimiento');?>
                            <div class="form-group">
                                <label for="tipomantenimiento">
                                    <b>Tipo de Mantenimiento *</label>
                                <select class="form-control" id="tipomantenimiento" name="Man_Tipo">
                                    <option>Preventivo</option>
                                    <option>Correctivo</option>
                                    <option>Emergencia</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="fecha">Fecha del Mantenimiento *</label>
                                <?php echo form_error('Man_Fecha'); ?>
                                <input type="date" placeholder="" class="form-control" name="Man_Fecha" value= "<?php  echo  set_value ( 'Man_Fecha' );  ?>">
                            </div>
                            
                            <div class="form-group">
                                <label for="tiempo">Tiempo de Mantenimiento *</label>
                                <?php echo form_error('Man_Tiempo'); ?>
                                <input type="text" placeholder="" class="form-control" name="Man_Tiempo" value= "<?php  echo  set_value ( 'Man_Tiempo' );  ?>">
                            </div>

                            <div class="form-group">
                                <label for="Comentarios">Comentario Inicial *</label>
                                <?php echo form_error('Man_Comentario_Inicial'); ?>
                                <textarea class="form-control" id="Comentarios" rows="3" name="Man_Comentario_Inicial" value= "<?php  echo  set_value ( 'Man_Comentario_Inicial' );  ?>"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="zonacomun">
                                    <b>Zona Común *</label>
                                    <?php echo form_error('zona_comun'); ?>
                                    <select class="form-control" name="zona_comun">
                                        <option value="">Seleccione</option>
                                        <?php foreach($listadoz as $listar): ?>
                                    <option value="<?php echo $listar->Zon_Com_Id;?>"><?php echo $listar->Zon_Com_Nombre;?></option>
                                    <?php endforeach; ?> 
                                    </select>
                            </div>
                            <div class="form-group">
                                <label for="proveedor">
                                    <b>Proveedor *</label>
                                    <?php echo form_error('proveedor'); ?>
                                    <select class="form-control" name="proveedor">
                                        <option value="">Seleccione</option>
                                        <?php foreach($listadop as $listar): ?>
                                    <option value="<?php echo $listar->Pro_Id;?>"><?php echo $listar->Pro_Nombre;?></option>
                                    <?php endforeach; ?> 
                                    </select>
                            </div>
                            

                            <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary" onclick="form.submit();this.disabled=true">Registrar</button>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <?php $this->load->view('footer'); ?>