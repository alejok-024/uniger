<?php $this->load->view('header'); ?>
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">
        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item active">Registro</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Zonas Comunes</h4>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <br>
                    <div class="card m-b-30">
                        <div class="card-header">
                                <h3>Registro de zona común</h3>
                                <a>Los campos marcados con * son obligatorios</a>
                        </div>
                        <div class="card-body">
                            <?php echo form_open('ZonaComun/crear_zonacomun');?>
                            <div class="form-group">
                                <label >Nombre de Zona *</label>
                                <?php echo form_error('Zon_Com_Nombre'); ?>
                                <input type="text" placeholder="" class="form-control" name="Zon_Com_Nombre" value= "<?php  echo  set_value ( 'Zon_Com_Nombre' );  ?>">
                            </div>
                            <div class="form-group">
                                <label >Periodo de Mantenimiento *</label>
                                <?php echo form_error('Zon_Periodo_Mantenimiento'); ?>
                                <input type="text" placeholder="" class="form-control" name="Zon_Periodo_Mantenimiento" value= "<?php  echo  set_value ( 'Zon_Periodo_Mantenimiento' );  ?>">
                            </div>
                            <div class="form-group">
                                <label >Descripción *</label>
                                <?php echo form_error('Zon_Descripcion'); ?>
                                <textarea class="form-control" id="Descripcion" rows="3" name="Zon_Descripcion" value= "<?php  echo  set_value ( 'Zon_Descripcion' );  ?>"></textarea>
                            </div>
                            <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary" onclick="form.submit();this.disabled=true">Registrar</button>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <?php $this->load->view('footer'); ?>