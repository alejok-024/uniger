<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>UNIGER | UNIGER</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Themesdesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Iconos -->
    <link rel="shortcut icon" href="<?php echo base_url('application/views/'); ?>assets/images/favicon.ico">

    <!-- Css -->
    <link href="<?php echo base_url('application/views/'); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('application/views/'); ?>assets/css/style.css" rel="stylesheet" type="text/css" />

</head>


    <body>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center mt-0 m-b-15">
                        <a href="index.html" class="logo logo-admin"><img src="<?php echo base_url('application/views/'); ?>assets/images/logo-dark.png" height="30" alt="logo"></a>
                    </h3>
                    <?php 
                                if(isset($_GET['success']))
                                {?>
                                    <div class="alert alert-success" role="alert">
                                        <strong>¡Usuario registrado!</strong> Se le enviará un correo cuando el administrador verifique su solicitud de ingreso.
                                    </div>                                    
                                <?php }
                            ?>

                    <h4 class="text-muted text-center font-18"><b>Registro</b></h4>

                    <div class="p-3">
                        <?php echo form_open('Security/registro'); ?>
                            <div class="form-group row">
                                <div class="col-12">
                                <?php echo form_error('Usu_Nombres'); ?>
                                    <input class="form-control" type="text" required="" placeholder="Nombres" name="Usu_Nombres" value="<?php echo set_value('Usu_Nombres'); ?>"> 
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12">
                                <?php echo form_error('Usu_Apellidos'); ?>
                                    <input class="form-control" type="text" required="" placeholder="Apellidos" name="Usu_Apellidos" value="<?php echo set_value('Usu_Apellidos'); ?>"> 
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12">
                                <?php echo form_error('Usu_Telefono'); ?>
                                    <input class="form-control" type="number" required="" placeholder="Teléfono" name="Usu_Telefono" value="<?php echo set_value('Usu_Telefono'); ?>"> 
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12">
                                <?php echo form_error('Usu_Apto'); ?>
                                    <input id="regAptoInput" class="form-control" type="number" required="" placeholder="Apartamento" name="Usu_Apto" value="<?php echo set_value('Usu_Apto'); ?>" max="999" min="100"> 
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                <?php echo form_error('Usu_Email'); ?>
                                    <input class="form-control" type="email" required="" placeholder="Correo" name="Usu_Email" value="<?php echo set_value('Usu_Email'); ?>"> 
                                    <small id="passwordHelpInline" class="text-muted">
                                    Este será su usuario y no podrá ser modificado.
                                    </small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                <?php echo form_error('Usu_Contrasena'); ?>
                                    <input class="form-control" type="password" required="" placeholder="Contraseña" name="Usu_Contrasena"> 
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                <?php echo form_error('Usu_ReContrasena'); ?>
                                    <input class="form-control" type="password" required="" placeholder="Repita su contraseña" name="Usu_ReContrasena"> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                <?php echo form_error('Usu_Comentario'); ?>
                                    <textarea class="form-control" required="" placeholder="Información extra de contacto, por qué desea ingresar a la plataforma, entre otros" name="Usu_Comentario"><?php echo set_value('Usu_Comentario'); ?></textarea> 
                                </div>
                            </div>
                            <!--
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1" required="">
                                        <label class="custom-control-label font-weight-normal" for="customCheck1">Acepto los <a href="#" class="text-muted">Terminos y Condicioness</a></label>
                                    </div>
                                </div>
                            </div>
                            -->
                            <div class="form-group text-center row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-info btn-block waves-effect waves-light" type="submit">Registrarse</button>
                                </div>
                            </div>

                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20 text-center">
                                    <a href="<?php echo base_url(); ?>" class="text-muted">¿Ya tienes una cuenta?</a>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>

                </div>
            </div>
        </div>


        <!-- jQuery  -->
        <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/modernizr.min.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/jquery.scrollTo.min.js"></script>
        <!-- Alertify js -->
        <script src="<?php echo base_url('application/views/'); ?>assets/plugins/alertify/js/alertify.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/pages/alertify-init.js"></script>

        <!-- Javascript -->
        <script src="<?php echo base_url('application/views/'); ?>assets/js/app.js"></script>
        <script src="<?php echo base_url('application/views/'); ?>assets/js/ajax.js"></script>
        <script>var base_url = "<?php echo base_url(); ?>";</script>

</body>
</html>