<?php $this->load->view('header') ;?>
<div class="wrapper">
    <!-- container -->
    <div class="container-fluid">

        <!-- Titulo Página -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Acerca De</a>
                            </li>
                        </ol>
                    </div>
                    <h1 class="page-title">Acerca De</h1>
                </div>
            </div>
        </div>
        <div class="card s-m-30">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                    <h6>¿Que es UNIGER?</h6>
                    <p>UNIGER permite tener el manejo y control de los procesos de facturación, cobranza, gestión y mantenimiento preventivo y correctivo que se lleva a cabo en la copropiedad ya que unificará la gestión de todos los procesos en una única plataforma, de fácil acceso y uso para todos los usuarios.</p>
                    <br>
                    <h6>¿Porque se creo UNIGER?</h6>
                    <p>Actualmente el cliente conjunto residencial Torres de la Giralda, ubicada en la carrera 39 # 47 – 53, cuenta con 3 torres, 180 unidades de vivienda, gimnasio y turco, realiza los procesos de facturación, cobranza, gestión de infraestructura y mantenimiento de la propiedad a través de correos electrónicos, llamadas, whatsapp, incluso documentos físicos, todo esto se debe a que no hay una única forma para la gestión y recepción de la información; lo cual trae desorden, poco control, limitada trazabilidad, confusiones, reproceso y pérdida de información.</p>
                    <br>
                    <h6>UNIGER cuenta con los siquientes modulos: </h6>
                    <p><b>Inmuebles: </b>El módulo de inmuebles se encarga de gestionar toda la información relacionada con los apartamentos, cuarto útil y parqueadero del conjunto residencial Torres de la Giralda, el rol administrador es el único que tiene acceso a todas las funciones de este módulo.</p>
                    <p><b>Recibos: </b>El módulo recibos es el encargado de gestionar los recibos de pago de administración de cada uno de los inmuebles y los abonos que se pueden hacer a estos pagos, el rol administrador es el único que tiene acceso a todas las funciones de este módulo.</p>
                    <p><b>Zonas comunes: </b>El módulo de zonas comunes se encarga de gestionar toda la información relacionada con las reservas, los mantenimientos, las zonas comunes y los proveedores del conjunto residencial Torres de la Giralda, el rol administrador tiene acceso a todas las funciones de este módulo y el rol usuario solo puede acceder a ciertas funciones.</p>
                    <p><b>Comunicaciones: </b>El módulo de comunicaciones permite a los usuarios realizar publicaciones o denuncias, también permite al administrador generar notificaciones (Mensajes), lo cual permite la comunicación directa entre el administrador y residentes del conjunto residencial.</p>
                    <p><b>Usuarios: </b>El módulo usuarios es el encargado de gestionar los roles y los permisos que se le asigna a cada uno de los usuarios registrados dentro de la plataforma, además de generar reportes en cuanto a los registros y los diferentes tipos de perfiles que maneja el software, el rol administrador es el único que tiene acceso a todas las funciones de este módulo.</p>
                    </div>
                </div>
            </div>
        </div>    

        
    <?php $this->load->view('footer'); ?>

