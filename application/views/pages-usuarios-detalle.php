
<?php $this->load->view('header'); ?>


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Usuarios</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Administración de usuarios</a>
                            </li>
                            <li class="breadcrumb-item active">Datos de usuarios</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Datos de usuario</h4>
                </div>
            </div>
        </div>
        <?php $Usuario = $Usuario[0]; ?>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-12">
                                <div class="invoice-title">
                                    <h4 class="pull-right font-16">
                                        <strong>Editar: <?php echo $Usuario->Usu_Nombres." ".$Usuario->Usu_Apellidos; ?> </strong>
                                    </h4>
                                    <h3 class="m-t-0">
                                        <img src="<?php echo base_url('application/views/'); ?>assets/images/logo-dark.png" alt="logo" height="32" />
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <?php echo form_open('Usuarios/actualizarUsuario');?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="p-20">
                                            
                                                    <div class="form-group">
                                                        <label>Nombres *</label>
                                                        <input type="text" placeholder="" name="nombres" value="<?php echo $Usuario->Usu_Nombres; ?>" class="form-control">
                                                        <?php echo form_error('nombres'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tipo de documento *</label>
                                                        <select name="tipo_documento" id="" class="form-control">
                                                        <?php
                                                            if ($Usuario->Usu_Tipo_Documento == "Cédula Ciudadania") {?>
                                                                <option value="Cédula Ciudadania">Cédula Ciudadania</option>
                                                                <option value="Tarjeta de Identida">Tarjeta de Identidad</option>
                                                                <option value="Cédula Extranjería">Cédula Extranjería</option>
                                                                <option value="Visa">Visa</option>   
                                                            <?php
                                                                }elseif($Usuario->Usu_Tipo_Documento == "Tarjeta de Identida"){
                                                            ?>
                                                                    <option value="Tarjeta de Identida">Tarjeta de Identidad</option>
                                                                    <option value="Cédula Ciudadania">Cédula Ciudadania</option>
                                                                    <option value="Cédula Extranjería">Cédula Extranjería</option>
                                                                    <option value="Visa">Visa</option>   
                                                            <?php
                                                                }elseif($Usuario->Usu_Tipo_Documento == "Cédula Extranjería"){
                                                            ?>
                                                                    <option value="Cédula Extranjería">Cédula Extranjería</option>
                                                                    <option value="Cédula Ciudadania">Cédula Ciudadania</option>
                                                                    <option value="Tarjeta de Identida">Tarjeta de Identidad</option>
                                                                    <option value="Visa">Visa</option>   
                                                            <?php }else{
                                                            ?>
                                                                <option value="Visa">Visa</option>   
                                                                <option value="Cédula Ciudadania">Cédula Ciudadania</option>
                                                                <option value="Tarjeta de Identida">Tarjeta de Identidad</option>
                                                                <option value="Cédula Extranjería">Cédula Extranjería</option>
                                                        <?php } ?>  
                                                        </select>
                                                        <?php echo form_error('tipo_documento'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fecha nacimiento *</label>
                                                        <input type="date" placeholder="" name="fecha_nacimiento" value="<?php echo $Usuario->Usu_Fecha_Nacimiento ?>" class="form-control">
                                                        <?php echo form_error('fecha_nacimiento'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input disabled type="text" value="<?php echo $Usuario->Usu_Email; ?>" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Rol Principal *</label>
                                                        <select name="tipo_perfil" id="" class="form-control">
                                                        <?php
                                                            if ($Usuario->Usu_Rol = 1) {?>
                                                                <option value="0">Administrador</option>
                                                                <option value="1">Usuario</option>
                                                        <?php
                                                            }else{
                                                        ?>
                                                                <option value="1">Usuario</option>
                                                                <option value="0">Administrador</option>
                                                        <?php   
                                                            }
                                                        ?>
                                                        </select>
                                                        <?php echo form_error('tipo_perfil'); ?>
                                                    </div>

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="p-20">

                                                    <div class="form-group">
                                                        <label>Apellidos *</label>
                                                        <input type="text" placeholder="" name="apellidos" value="<?php echo $Usuario->Usu_Apellidos; ?>" class="form-control">
                                                        <?php echo form_error('apellidos'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Número documento *</label>
                                                        <input type="number" placeholder="" class="form-control" name="numero_documento" value="<?php echo $Usuario->Usu_Numero_Documento; ?>">
                                                        <?php echo form_error('numero_documento'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Teléfono *</label>
                                                        <input type="number" step="any" placeholder="" class="form-control" min="1" name="telefono" value="<?php echo $Usuario->Usu_Telefono_Celular; ?>">
                                                        <?php echo form_error('telefono'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fecha registro</label>
                                                        <input disabled placeholder="" class="form-control" value="<?php echo $Usuario->Usu_Fecha_Registro; ?>">
                                                        <input type="hidden" value="<?php echo $Usuario->Usu_Id; ?>" name="id_usuario">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Estado *</label>
                                                        <select name="estado" id="" class="form-control">
                                                        <?php
                                                            if ($Usuario->Usu_Fecha_Registro = "Aprobado") {?>
                                                                <option value="Aprobado">Aprobado</option>
                                                                <option value="Suspendido">Suspendido</option>
                                                                <option value="Cancelado">Cancelado</option>
                                                        <?php
                                                            }elseif($Usuario->Usu_Fecha_Registro = "Suspendido"){
                                                        ?>
                                                                <option value="Suspendido">Suspendido</option>
                                                                <option value="Aprobado">Aprobado</option>
                                                                <option value="Cancelado">Cancelado</option>
                                                        <?php
                                                            }else{
                                                        ?>
                                                                <option value="Cancelado">Cancelado</option>  
                                                                <option value="Suspendido">Suspendido</option>
                                                                <option value="Aprobado">Aprobado</option>
                                                            <?php } ?>
                                                        </select>
                                                        <?php echo form_error('estado'); ?>
                                                    </div>

                                            </div>
                                        </div> <!-- end col -->

                                    </div>
                                    <div class="row">
                                        <div class="offset-md-5">
                                            <a class="btn btn-primary waves-effect waves-light" href="<?php echo base_url('usuarios/detallesinmuebles/'.$Usuario->Usu_Id); ?>">Inmuebles</a>
                                            <button type="submit" class="btn btn-primary">Actualizar</button>
                                        </div>                                    
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!-- modal -->
                                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Abonos</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Tabla -->
                                                    <table class="table table-striped">
                                                        <!-- Encabezado -->
                                                        <thead>
                                                            <tr>
                                                                <td>Apto</td>
                                                                <td>Área construida</td>
                                                                <td>Coeficiente de propiedad</td>
                                                                <td>Rol del Usuario</td>
                                                                <td>Ver Detalles</td>
                                                                
                                                            </tr>
                                                        </thead><!-- Fin Encabezado -->
                                                        <!-- Cuerpo -->
                                                        <tbody>
                                                            <?php
                                                            foreach ($Inmuebles as $Inmueble) 
                                                            {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $Inmueble->Inm_Id; ?></td>
                                                                    <td><?php echo $Inmueble->Inm_Area_Construida; ?></td>
                                                                    <td><?php echo $Inmueble->Inm_Coeficiente_Propiedad; ?></td>
                                                                    <td><?php echo $Inmueble->InmXUsu_Rol; ?></td>
                                                                    <td>
                                                                        <a href="#" class="btn btn-outline-info waves-effect waves-light">
                                                                        <i class="mdi mdi-eye"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            <?php
                                                            } 
                                                            ?>
                                                        </tbody>
                                                    </table><!-- Fin Tabla -->
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- end modal -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- end wrapper -->
<!-- Footer -->
<?php $this->load->view('footer'); ?>
<?php 
    $success = $this->session->flashdata('success');
    if ($success) 
    {
    ?>
    <script>
        $(document).ready(function(){
            alertify.success("Usuario actualizado con éxito");
        });
    </script>   
    <?php
    }
?>

