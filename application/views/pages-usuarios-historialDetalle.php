
<?php $this->load->view('header'); ?>


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Usuarios</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Peticiones de ingreso</a>
                            </li>
                            <li class="breadcrumb-item active">Historial de registros</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Datos</h4>
                </div>
            </div>
        </div>
        <?php $historial = $historial[0]; ?>
      <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-12">
                                <div class="invoice-title">
                                    <h4 class="pull-right font-16">
                                        <strong>Datos: <?php echo $historial->Reg_Nombres." ".$historial->Reg_Apellidos; ?> </strong>
                                    </h4>
                                    <h3 class="m-t-0">
                                        <img src="<?php echo base_url('application/views/'); ?>assets/images/logo-dark.png" alt="logo" height="32" />
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <label>Nombre</label>
                                    <input type="text" disabled name="fecha_nacimiento" value="<?php echo $historial->Reg_Nombres ?>" class="form-control">
                         
                        </div>
                        <div class="col-md-6">
                                        <label>Apellidos</label>
                                    <input type="text" disabled name="fecha_nacimiento" value="<?php echo $historial->Reg_Apellidos ?>" class="form-control">
                        </div>
                        
                    </div>
                    </div>
                    <div class="card-body">
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <label>Telefono</label>
                                    <input type="text" disabled name="fecha_nacimiento" value="<?php echo $historial->Reg_Telefono ?>" class="form-control">
                         
                        </div>
                        <div class="col-md-6">
                                        <label>Correo</label>
                                    <input type="text" disabled name="fecha_nacimiento" value="<?php echo $historial->Reg_Email ?>" class="form-control">
                        </div>
                    </div>
                    </div>
                    <div class="card-body">
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <label>Apartamento</label>
                                    <input type="text" disabled name="fecha_nacimiento" value="<?php echo $historial->Reg_Apto ?>" class="form-control">
                         
                        </div>
                        <div class="col-md-6">
                                        <label>Estado</label>
                                    <input type="text" disabled name="fecha_nacimiento" value="<?php echo $historial->Reg_Estado ?>" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                        <label>Fecha de registro</label>
                                    <input type="text" disabled name="fecha_nacimiento" value="<?php echo $historial->Reg_Fecha_Registro ?>" class="form-control">
                         
                        </div>
                        <div class="col-md-6">
                                        <label>Comentario</label>
                                    <input type="text" disabled name="fecha_nacimiento" value="<?php echo $historial->Reg_Comentario ?>" class="form-control">
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</div><!-- end wrapper -->
<!-- Footer -->
<?php $this->load->view('footer'); ?>
