
<?php $this->load->view('header'); ?>

<?php $usu_id = $this->uri->segment(4); ?>

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Facturacion</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Usuario de pago</a>
                            </li>
                            <li class="breadcrumb-item active">Recibo</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Recibo de pago</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="offset-3 col-6">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo form_open('usuarios/actualizarInmueblesxUsuario'); ?>
                                    <div class="form-group">
                                        <label for="rol">Rol actual: <?php echo $inmuebles->InmXUsu_Rol; ?></label>
                                        <select name="rol" class="form-control" id="rol">
                                            <option value="">Seleccione nuevo rol</option>
                                            <option value="Dueño">Dueño</option>
                                            <option value="Inquilino">Inquilino</option>
                                            <option value="Arrendatario">Arrendatario</option>
                                            <option value="Comodatario">Comodatario</option>
                                            <option value="Albacea">Albacea</option>
                                        </select>
                                        <?php echo ($this->session->flashdata('success')) ? "<div style='color:red'>".$this->session->flashdata('success')."</div>" : ""; ?>
                                        <input type="hidden" name="Inm_Id" value="<?php echo $inmuebles->Inm_Id; ?>">
                                        <input type="hidden" name="Usu_Id" value="<?php echo $usu_id; ?>">
                                    </div> 
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Actualizar</button>
                                    </div> 
                                <?php echo form_close();  ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- end wrapper -->
<!-- Footer -->
<?php $this->load->view('footer'); ?>
<?php 
    $success = $this->session->flashdata('success');
    if ($success) 
    {
    ?>
    <script>
        $(document).ready(function(){
            alertify.success("Usuario actualizado con éxito");
        });
    </script>   
    <?php
    }
?>

