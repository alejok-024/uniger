
<?php $this->load->view('header'); ?>

<?php $usu_id = $this->uri->segment(3); ?>
<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item">
                                <a href="#">Inicio</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Facturacion</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Usuario de pago</a>
                            </li>
                            <li class="breadcrumb-item active">Recibo</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Recibo de pago</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="offset-3 col-6">
                <div class="card m-b-30">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Apto</th>
                                            <th>Rol</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($inmuebles as $dato) { ?>
                                        <tr>
                                            <td><?php echo $dato->Inm_Id; ?></td>
                                            <td><?php echo $dato->InmXUsu_Rol; ?></td>
                                            <td>
                                                <a class="btn btn-primary edit-inm-usu" href="<?php echo base_url('Usuarios/editarDetallesInmuebles/'.$dato->Inm_Id.'/'.$usu_id); ?>">Editar</a>
                                                <a class="btn btn-danger" href="">Eliminar</a>
                                            </td>
                                        </tr>                                            
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- end wrapper -->

<!-- sample modal content -->
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Editar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <?php $args = array('id' => 'form-update'); ?>
            <?php echo form_open('Usuarios/actualizarInmueblesxUsuario', $args); ?>
                <div class="modal-body">
                    <h4>Rol actual: <span id="rol-input"></span></h4>
                    
                    <div class="form-group">
                        <label for="">Apartamento</label>
                        <input id="apto-input" type="text" class="form-control" disabled>
                    </div>  
                    <div class="form-group">
                        <div class="alert alert-danger print-error-msg" style="display:none"></div>
                        <label for=rol"">Rol</label>
                        <select name="rol" class="form-control" id="rol">
                            <option value="">Seleccione</option>
                            <option value="Dueño">Dueño</option>
                            <option value="Inquilino">Inquilino</option>
                            <option value="Arrendatario">Arrendatario</option>
                            <option value="Comodatario">Comodatario</option>
                            <option value="Albacea">Albacea</option>
                        </select>
                    </div>  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Guardar Cambios</button>
                </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Footer -->
<?php $this->load->view('footer'); ?>
<?php 
    $success = $this->session->flashdata('success');
    if ($success) 
    {
    ?>
    <script>
        $(document).ready(function(){
            alertify.success("Usuario actualizado con éxito");
        });
    </script>   
    <?php
    }
?>

