<?php $this->load->view('header') ;?>
  <!-- wrapper -->
  <div class="wrapper">
        <!-- Contenedor -->
        <div class="container-fluid">

            <!-- Titulo Página -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item">
                                    <a href="#">Inicio</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="#">Usuarios</a>
                                </li>
                                <li class="breadcrumb-item active">Peticiones de Ingreso</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Usuarios Registrados</h4>
                    </div>
                </div>
            </div>
            <!-- Fin titulo pagina y miga de pan -->
            <!-- Contenido Principal -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Tabla -->      
                            <table class="table table-striped" id="datatable">
                                <!-- Encabezado -->
                                <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Estado</th>
                                        <th>Detalles</th>
                                    </tr>
                                </thead>
                                <!-- Fin Encabezado -->
                                <!-- Cuerpo -->
                                <tbody>
                                <?php if(!empty($Usuarios)){ ?>
                                <?php foreach($Usuarios as $Usuario)
                                        {?>
                                        <tr>
                                            <th scope="row"><?php echo $Usuario->Usu_Nombres." ".$Usuario->Usu_Apellidos; ?></th>
                                            <td><?php echo $Usuario->Usu_Telefono_Celular; ?></td>
                                            <td><?php echo $Usuario->Usu_Email; ?></td>
                                            <td><?php echo $Usuario->Usu_Fecha_Registro; ?></td>
                                            <td>
                                            <a href="detalle/<?php echo $Usuario->Usu_Id; ?>" class="btn btn-outline-info waves-effect waves-light">
                                                <i class="mdi mdi-eye"></i>
                                            </a>
                                            <?php 
                                            if($Usuario->Usu_Rol == '' || $Usuario->Usu_Rol != 0 ){ ?>
                                                <a href="#" class="btn btn-outline-danger waves-effect waves-light" onclick="LanzarModal(<?php echo $Usuario->Usu_Id; ?>);">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                            <?php }
                                            ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                                <!-- Fin Cuerpo -->
                            </table>
                            <!-- Fin Tabla -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fin container -->

        <!-- MODAL DELETE -->
        <div class="modal fade" id="ModalDeleteMantenimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                        <div class="col-xl-10 col-lg-10 col-md-10">
                            <div class="form-group">
                                <p>¿Está seguro que desea eliminar el proveedor?</p>
                            </div>
                            
                            <input type="text" class="form-control" name="TokenDel" id="TokenDel" hidden>
                                
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="EliminarProveedor();">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    </div>
    <!-- Fin wrapper -->
<script>
    var base_url = "<?php echo base_url();?>";
</script>


<script type="text/javascript">

function LanzarModal(ID){

    document.getElementById("TokenDel").value = ID;
    $('#ModalDeleteMantenimiento').modal('show');
}


function EliminarProveedor(){

    var TokenDel = document.getElementById("TokenDel");

    var Registro = {
        "Res_Token": TokenDel.value
    };

    $.ajax({
        url: '<? echo base_url('usuarios/eliminar');?>',
        method: 'POST',
        data: Registro,
        success: function(response)
        {
            
            $('#ModalDeleteMantenimiento').modal('hide');
            window.location = '/uniger/usuarios/Listar/';

        },
        error: function (err) {
            alert("Ocurrió un error: " + JSON.stringify(err, null, 2));
        }
    });
    }

</script>

        <!-- Required datatable js -->
        <?php $this->load->view('footer'); ?>
<?php if ($this->session->flashdata('result')) {?>
    <script>
        $(document).ready(function(){
            alertify.success("Proceso realizado con éxito");
        });
    </script>   
<?php }; ?>